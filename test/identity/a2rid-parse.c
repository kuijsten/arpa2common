/* Test parser for [non-ARPA2] Remote Identities.
 *
 * SPDX-License-Identifier: BSD-2-Clause 
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl> 
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <arpa2/identity.h>
#include <arpa2/except.h>


bool test (char *instr) {
	a2id_t trid;
	bool ok1 = a2id_parse_remote (&trid, instr, strlen (instr));
	log_debug ("%s - %s", ok1 ? "OK" : "KO", instr);
	log_debug ("   = %s", trid.txt);
	time_t expts = (18000 + trid.expireday) * 86400;
	struct tm expdy;
	char expdystr [105];
	gmtime_r (&expts, &expdy);
	strftime (expdystr, 102, "%F", &expdy);
	struct tm exptm;
	char exptmstr [105];
	gmtime_r (&trid.expiration, &exptm);
	strftime (exptmstr, 102, "%F", &exptm);
	log_debug ("   @ %s due to expireday=%d", expdystr, trid.expireday);
	log_debug ("   @ %s due to expiration (0 until a2id_verify()", exptmstr);
	if (ok1) a2sel_detail ("   ", &trid);
	a2id_t tid;
	bool ok2 = a2id_parse (&tid, instr, strlen (instr));
	log_debug ("%s - %s", ok2 ? "OK" : "KO", instr);
	log_debug ("   = %s", tid.txt);
	if (ok2) {
		int i = 0;
		while (i <= A2ID_OFS_USERID) {
			tid.ofs [i++] = 0;
		}
		while (i < A2ID_OFS_AT_DOMAIN) {
			tid.ofs [i++] = tid.ofs [A2ID_OFS_AT_DOMAIN];
		}
		tid.sigflags = 0;
		tid.expireday = ~0;
	}
	if (ok2) a2sel_detail ("   ", &tid);
	bool ok3 = (sizeof (trid) == sizeof (tid));
	ok3 = ok3 && (memcmp (&trid, &tid, sizeof (trid)) == 0);
	log_debug ("%s - same data from a2id_parse_remote() and flattened a2id_parse()",
			ok3 ? "OK" : "KO");
	if (!ok3) {
		log_data ("a2rid_parse() output", (uint8_t *) &trid, sizeof (trid), 75);
		log_data ("a2id__parse() output", (uint8_t *) &tid,  sizeof (tid ), 75);
	}
	return (ok1 && ok2 && ok3);
}

int main (int argc, char *argv []) {
	bool ok = true;
	a2id_init ();
	//
	// Test arguments
	if (argc < 2) {
		fprintf (stderr, "Usage: %s <user>@<domain>...\n", argv [0]);
		exit (1);
	}
	//
	// Iterate over all identities supplied
	for (int argi = 1; argi < argc; argi++) {
		if (!test (argv [argi])) {
			ok = false;
		}
	}
	//
	// Exit value
	return (ok ? 0 : 1);
}
