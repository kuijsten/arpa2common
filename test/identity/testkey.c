/* Generate a test keyfd.  Once.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <string.h>
#include <assert.h>

#include <unistd.h>
#include <limits.h>

#include "testkey.h"


/* Generate a keyfd.  The key is fixed, ideal for test purposes.
 * Can be fed directly into a2id_addkey().  Remember to close afterwards.
 */
int keyfd_test (void) {
	static int uses_left = 1;
	assert (uses_left-- > 0);
	// December 23rd, 2020.  256 bits in hex.  Newline.  76 chars in all.
	static char secret [] = "arpa2id,2.0.0,sigkey,sha256,NFWIAIOGDNEYD5GYNUXK64D7DEVUUTPNXRW4KCXQFCWYQHKQARWQB7M56CUMUUJAYYNXWOHKQREP6DXIECEMI7WM3WTJMXZPLT3F2VMHNZRXR7VIDFBDLI64FKXL4NMFO7VO2Q74OZ4EJY6GCTA3YWMCD6ZF6B6RHQCJF3SYSWANFGRMOI6Q\n";
	assert (sizeof (secret)-1 <= PIPE_BUF);
	int keyrw [2];
	assert (pipe (keyrw) == 0);
	assert (write (keyrw [1], secret, sizeof(secret)-1) == sizeof(secret)-1);
	memset (secret, 0, sizeof (secret));
	close (keyrw [1]);
	return keyrw [0];
}

