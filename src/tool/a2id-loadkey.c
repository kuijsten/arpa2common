/* ARPA2 Signed Identity Key Loader
 *
 * a2id-keyload <KEYFILE> <FD> <COMMAND>...
 *
 * Retrieve a key from a file and present it on a file descriptor.
 * Then continue to exec() the program in the remaining arguments.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <errno.h>

#include <arpa2/identity.h>


int main (int argc, char *argv []) {
	//
	// Parse arguments
	bool ok = true;
	ok = ok && (argc > 3);
	char *end = NULL;
	unsigned long fdnr_ul = strtoul (argv [2], &end, 10);
	int fdnr = fdnr_ul;
	ok = ok && (end != NULL) && (*end == '\0');
	ok = ok && (fdnr_ul ==  (unsigned long) fdnr);
	//
	// Produce helpful information if need be
	if (!ok) {
		fprintf (stderr, "Usage: %s <KEYFILE> <FDNR> <COMMAND>...\n", argv [0]);
		exit (1);
	}
	//
	// Test if the file descriptor is free
	struct stat st;
	if ((fstat (fdnr, &st) == 0) || (errno != EBADF)) {
		fprintf (stderr, "File descriptor is already open\n");
		exit (1);
	}
	//
	// Read the key file
	char keybuf [PIPE_BUF + 1];
	int keyfile = open (argv [1], O_RDONLY);
	if (keyfile < 0) {
		perror ("Failed to open key file");
		exit (1);
	}
	ssize_t keylen = read (keyfile, keybuf, PIPE_BUF + 1);
	if (keylen < 0) {
		perror ("Error reading from key file");
		exit (1);
	}
	if (keylen > PIPE_BUF) {
		fprintf (stderr, "Size of key file too large\n");
		exit (1);
	}
	close (keyfile);
	//
	// Construct the keypipe[] pair
	int keypipe [2];
	if (pipe (keypipe) != 0) {
		perror ("Failed to setup pipeline to new program");
		exit (1);
	}
	//
	// Pass the key to the write-side keypipe [1]
	ssize_t written = write (keypipe [1], keybuf, keylen);
	memset (keybuf, 0, sizeof (keybuf));
	if (written < 0) {
		perror ("Failed to write key");
		exit (1);
	} else if (written != keylen) {
		fprintf (stderr, "Not the complete key got written\n");
		exit (1);
	}
	close (keypipe [1]);
	//
	// Ensure that the read-side keypipe [0] is at fdnr
	if (keypipe [0] != fdnr) {
		if (dup2 (keypipe [0], fdnr) < 0) {
			perror ("Failed to move pipeline in sight");
			exit (1);
		}
		close (keypipe [0]);
		keypipe [0] = fdnr;
	}
	//
	// Now exec() the program
	argc -= 3;
	argv += 3;
	execvp (argv [0], argv);
	perror ("Failed to fork the commend");
	close (fdnr);
	exit (1);
}

