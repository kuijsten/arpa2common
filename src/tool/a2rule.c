/* tool/a2rule.c -- Tool to work on Policy Rules in a database.
 *
 * This tool can be used to update Policy Rules in the database,
 * for uses such as Communication Access Control and ARPA2 Groups.
 *
 * The tool should be installed as "a2rule" for administrative use.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#define _GNU_SOURCE	/* for strchrnul() */

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>

#include <unistd.h>
#include <regex.h>
#include <fcntl.h>

#include <lmdb.h>

#include <arpa2/except.h>
#include <arpa2/digest.h>
#include <arpa2/identity.h>
#include <arpa2/rules.h>
#include <arpa2/rules_db.h>
#include <arpa2/access.h>
#include <arpa2/access_comm.h>
#include <arpa2/access_document.h>
#include <arpa2/group.h>
#include <arpa2/util/snoprintf.h>

#include <arpa2/util/cmdparse.h>
#include "grammar.h"



/***** Grammars for commands *****/



static const char ruleset_usage [] = "...  Abstract management of rule sets\n"
	"ruleset add ...  To add    a rule that does not exist yet\n"
	"ruleset del ...  To remove a rule that matches exactly\n"
	"ruleset set ...  To have   a rule and drop any existing\n"
	"ruleset get ...  To query  a rule that matches well enough\n";

static const char ruleset_addset_usage [] = "[dbtrunk NUM] servicekey SECRET\n"
	"[type TYPE] name NAME [selector ARPA2SEL] rule RULE";

static const uint32_t ruleset_addset_combis [] = {
	/* Require DBSVCKEY, NAME and RULE */
	FLAG_DBSVCKEY | FLAG_NAME | FLAG_RULE,
	FLAG_DBSVCKEY | FLAG_NAME | FLAG_RULE,
	/* end marker */
	0
};

static const struct cmdparse_grammar ruleset_addset_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		NULL,		// dbsecret
		NULL,		// dbprefix
		NULL,		// dbdomkey
		"servicekey",	// servicekey
				// NO_objtype
		"name",		// local
		"selector",	// remote==selector
		NULL,		// iterate
		"type",		// rights==type
		"rule",		// list==rule
		NULL,		// alias
		NULL,		// endalias
		NULL,		// signflags
		NULL,		// newuserid
		NULL,		// newalias
		NULL,		// actorid
	},
	.listwords = FLAG_RULE,
	.combinations = ruleset_addset_combis,
};

static const char ruleset_delget_usage [] = "[dbtrunk NUM] [dbprefix #WORD] servicekey SECRET\n"
	"[type TYPE] name NAME [selector ARPA2SEL] [rule RULE]";

static const uint32_t ruleset_delget_combis [] = {
	/* Require DBSVCKEY and NAME */
	FLAG_DBSVCKEY | FLAG_NAME,
	FLAG_DBSVCKEY | FLAG_NAME,
	/* end marker */
	0
};

static const struct cmdparse_grammar ruleset_delget_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		NULL,		// dbsecret
		"dbprefix",	// dbprefix
		NULL,		// dbdomkey
		"servicekey",	// servicekey
				// NO_objtype
		"name",		// local
		"selector",	// remote==selector
		NULL,		// iterate
		"type",		// rights==type
		"rule",		// list==rule
		NULL,		// alias
		NULL,		// endalias
		NULL,		// signflags
		NULL,		// newuserid
		NULL,		// newalias
		NULL,		// actorid
	},
	.listwords = FLAG_RULE,
	.combinations = ruleset_delget_combis,
};


static const char comm_usage [] = "...  Manages Communication Access:\n"
	"comm add ...   Add a new rule, which must not match pre-existing ones\n"
	"comm set ...   Set a desired rule, replacing any pre-existing match\n"
	"comm get ...   Retrieve the rules that match the attributes\n"
	"comm del ...   Delete those rules that match the attributes";

static const char comm_addset_usage [] = "[dbtrunk NUM] [dbdomkey SECRET]\n"
	"local ARPA2ID remote ARPA2SEL (list LIST|rights LETTERS)\n"
	"[alias|endalias ALIAS] [signflags LETTERS]\n"
	"[newuserid NEWUSERID] [newalias ALIAS] [actorid ACTORID]";

static const uint32_t comm_addset_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* Require LOCAL, REMOTE */
	FLAG_LOCAL | FLAG_REMOTE,	FLAG_LOCAL | FLAG_REMOTE,
	/* Allow ALIAS or ENDALIAS, but not both */
	FLAG_ALIAS | FLAG_ENDALIAS,	0,
	FLAG_ALIAS | FLAG_ENDALIAS,	FLAG_ALIAS,
	FLAG_ALIAS | FLAG_ENDALIAS,	FLAG_ENDALIAS,
	/* Require LIST or RIGHTS, but not both */
	FLAG_LIST | FLAG_RIGHTS,	FLAG_LIST,
	FLAG_LIST | FLAG_RIGHTS,	FLAG_RIGHTS,
	/* end marker */
	0
};

static const struct cmdparse_grammar comm_addset_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		NULL,		// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"local",	// local
		"remote",	// remote
		NULL,		// iterate
		"rights",	// rights
		"list",		// list
		"alias",	// alias
		"endalias",	// endalias
		"signflags",	// signflags
		"newuserid",	// newuserid
		"newalias",	// newalias
		"actorid",	// actorid
	},
	.listwords = 0,
	.combinations = comm_addset_combis,
};

static const char comm_delget_usage [] = "[dbtrunk NUM] [dbdomkey SECRET] [dbprefix #WORD]\n"
	"local ARPA2ID remote ARPA2SEL [alias|endalias ALIAS] [signflags LETTERS]";

static const uint32_t comm_delget_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* Require LOCAL, REMOTE */
	FLAG_LOCAL | FLAG_REMOTE,	FLAG_LOCAL | FLAG_REMOTE,
	/* Allow ALIAS or ENDALIAS, but not both */
	FLAG_ALIAS | FLAG_ENDALIAS,	0,
	FLAG_ALIAS | FLAG_ENDALIAS,	FLAG_ALIAS,
	FLAG_ALIAS | FLAG_ENDALIAS,	FLAG_ENDALIAS,
	/* end marker */
	0
};

static const struct cmdparse_grammar comm_delget_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		"dbprefix",	// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"local",	// local
		"remote",	// remote
		NULL,		// iterate
		NULL,		// rights
		NULL,		// list
		"alias",	// alias
		"endalias",	// endalias
		"signflags",	// signflags
		NULL,		// newuserid
		NULL,		// newalias
		NULL,		// actorid
	},
	.listwords = 0,
	.combinations = comm_delget_combis,
};


/* Support for Document Access changes:
 *
 * a2rule docu add|set domain <domain> collection <uuid>
 *        remote <selector>
 *        rights <letters> [actorid <actorid>]
 *
 * a2rule docu add|set domain <domain>
 *        volume <volume> [path <path>]
 *        remote <selector>
 *        rights <letters> [actorid <actorid>]
 *
 * a2rule docu del|get domain <domain> collection <uuid>
 *        remote <selector>
 *
 * a2rule docu del|get domain <domain>
 *        volume <volume> [path <path>]
 *        remote <selector>
 */

static const char docu_usage [] = "... Manages Document Access:\n"
	"docu add ...  Adds access to a document for a given remote\n"
	"docu set ...  Sets remote document access as desired, replacing any old setting\n"
	"docu get ...  Retrieve access information for a remote to a document\n"
	"docu del ...  Delete   access information for a remote on a document";

static const char docu_addset_usage [] = "[dbtrunk NUM] [dbdomkey SECRET]\n"
	"domain LOCAL.DOMAIN ( collection UUID | volume VOLUME [path PATH] )\n"
	"remote RUSER@REMOTE.DOMAIN rights RIGHTS [actorid ACTORID]";

static const uint32_t docu_addset_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* always require DOMAIN, REMOTE, RIGHTS */
	FLAG_DOMAIN | FLAG_REMOTE | FLAG_RIGHTS,
	FLAG_DOMAIN | FLAG_REMOTE | FLAG_RIGHTS,
	/* require COLLECTION or VOLUME.  COLLECTION forbids PATH */
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	FLAG_COLLECTION                          ,
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	                  FLAG_VOLUME            ,
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	                  FLAG_VOLUME | FLAG_PATH,
	/* end marker */
	0
};

static const struct cmdparse_grammar docu_addset_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		NULL,		// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"domain",	// local==domain
		"remote",	// remote
		"collection",	// iterate==collection
		"rights",	// rights
		NULL,		// list
		NULL,		// alias
		NULL,		// endalias
		"path",		// signflags==path
		NULL,		// newuserid
		"volume",	// newalias==volume
		"actorid",	// actorid
	},
	.listwords = 0,
	.combinations = docu_addset_combis,
};

static const char docu_delget_usage [] = "[dbtrunk NUM] [dbdomkey SECRET] [dbprefix #WORD]\n"
	"domain LOCAL.DOMAIN ( collection UUID | volume VOLUME [path PATH] )\n"
	"remote RUSER@REMOTE.DOMAIN";


static const uint32_t docu_delget_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* always require DOMAIN, REMOTE */
	FLAG_DOMAIN | FLAG_REMOTE,
	FLAG_DOMAIN | FLAG_REMOTE,
	/* require COLLECTION or VOLUME.  COLLECTION forbids PATH */
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	FLAG_COLLECTION                          ,
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	                  FLAG_VOLUME            ,
	FLAG_COLLECTION | FLAG_VOLUME | FLAG_PATH,
	                  FLAG_VOLUME | FLAG_PATH,
	/* and, because we added PATH to the goal, allow it in any form */
	FLAG_PATH,
	0,
	FLAG_PATH,
	FLAG_PATH,
	/* end marker */
	0
};

static const struct cmdparse_grammar docu_delget_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		"dbprefix",	// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"domain",	// local==domain
		"remote",	// remote
		"collection",	// iterate==collection
		NULL,		// rights
		NULL,		// list
		NULL,		// alias
		NULL,		// endalias
		"path",		// signflags==path
		NULL,		// newuserid
		"volume",	// newalias==volume
		NULL,		// actorid
	},
	.listwords = 0,
	.combinations = docu_delget_combis,
};



/* Support for Group Member changes:
 *
 * `a2rule group add member cooks+vegan@example.com identity johnny@johannus.kerk marks RWA`
 * `a2rule group add member cooks+rawfood@example.com identity marietje@mariakerk.redcross marks RW`
 * `a2rule group set member cooks+rawfood@example.com identity marietje@mariakerk.redcross marks RW`
 * `a2rule group set member cooks+rawfood@example.com identity marietje@mariakerk.rodekruis`
 * `a2rule group set member cooks+rawfood@example.com marks RWA`
 * `a2rule group get member cooks+vegan@example.com`
 * `a2rule group get member cooks+rawfood@example.com`
 * `a2rule group del member cooks+rawfood@example.com`
 */

static const char group_usage [] = "...  Manages Members of ARPA2 Groups:\n"
	"group add ...   Adds a new member, which must not match pre-existing ones\n"
	"group set ...   Sets a member as desired, replacing any pre-existing match\n"
	"group get ...   Retrieve the member that matches the attributes\n"
	"group del ...   Delete those member that matches the attributes";

static const char group_addset_usage [] = "[dbtrunk NUM] [dbdomkey SECRET]\n"
	"member GROUP+MEMBER@DOMAIN identity RUSER@REMOTE.DOMAIN marks MARKS";

static const uint32_t group_addset_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* Require MEMBER and IDENTITY */
	FLAG_MEMBER | FLAG_IDENTITY,	FLAG_MEMBER | FLAG_IDENTITY,
	/* Require MARKS */
	FLAG_MARKS,			FLAG_MARKS,
	/* end marker */
	0
};

static const struct cmdparse_grammar group_addset_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		NULL,		// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"member",	// local==member
		"identity",	// remote==identity
		NULL,		// iterate
		"marks",	// rights==marks
		NULL,		// list
		NULL,		// alias
		NULL,		// endalias
		NULL,		// signflags
		NULL,		// newuserid
		NULL,		// newalias
		NULL,		// actorid
	},
	.listwords = 0,
	.combinations = group_addset_combis,
};


static const char group_delget_usage [] = "[dbtrunk NUM] [dbdomkey SECRET] [dbprefix #WORD]\n"
	"member GROUP+MEMBER@DOMAIN";

static const uint32_t group_delget_combis [] = {
	/* Allow DBSECRET or DBDOMKEY, but not both */
	FLAG_DBSECRET | FLAG_DBDOMKEY,	0,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBSECRET,
	FLAG_DBSECRET | FLAG_DBDOMKEY,	FLAG_DBDOMKEY,
	/* Require MEMBER */
	FLAG_MEMBER,			FLAG_MEMBER,
	/* end marker */
	0
};

static const struct cmdparse_grammar group_delget_grammar = {
	.keywords = {
				// NO_dbenv
				// NO_dbname
		"dbtrunk",	// dbtrunk
		"dbsecret",	// dbsecret
		"dbprefix",	// dbprefix
		"dbdomkey",	// dbdomkey
		NULL,		// servicekey
				// NO_objtype
		"member",	// local==member
		NULL /*"identity"*/,	// remote==identity
		NULL,		// iterate
		NULL/*"marks"*/,	// rights==marks
		NULL,		// list
		NULL,		// alias
		NULL,		// endalias
		NULL,		// signflags
		NULL,		// newuserid
		NULL,		// newalias
		NULL,		// actorid
	},
	.listwords = 0,
	.combinations = group_delget_combis,
};



/***** Database routines *****/



/* Set operation to perform on (a rule in) a ruleset.
 * We usually ADD, SET, DEL or GET.  Listing rules in a
 * ruleset can make use of the GET set operation.
 */
enum opcode {
	OPCODE_ADD,
	OPCODE_SET,
	OPCODE_LAST_CREATOR,	/* marker for last with creator args */
	OPCODE_DEL,
	OPCODE_LAST_DBWRITER,	/* marker for last to write data */
	OPCODE_GET,
	OPCODE_OTHER,		/* marker for unknown subcommand */
};

/* Though opcode_t is intended to carry an enum opcode
 * value, it is mapped to short for compatibility with
 * "cmdparse.h" generic definitions.
 */
typedef short opcode_t;


/* Derived key printing, in hexadecimal form, on stdout.
 * These keys gradually become more specific due to the
 * additional hashing of data into the key, and can be
 * supplied to users at their respective entrance level.
 */
void a2rule_printkey (const char *prompt, rules_dbkey key) {
	printf ("%s", prompt);
	for (unsigned i = 0; i < sizeof (rules_dbkey); i++) {
		printf ("%02x", key [i]);
	}
	printf ("\n");
}


/* Parse a derived key in hexadecimal form and store in
 * the destination DBkey.
 *
 * Return true on success, false on syntax errors.
 */
bool a2rule_hexdigit (uint8_t *out, char in) {
	if ((in >= '0') && (in <= '9')) {
		*out = in - '0';
		return true;
	}
	in &= 0xdf;
	if ((in >= 'A') && (in <= 'F')) {
		*out = in - 'A' + 10;
		return true;
	}
	return false;
}
//
bool a2rule_parsekey (rules_dbkey dest, const char *hexkey) {
	bool ok = true;
	memset (dest, 0, sizeof (rules_dbkey));
	ok = ok && (strlen (hexkey) == 2 * sizeof (rules_dbkey));
	for (unsigned p = 0; p < sizeof (rules_dbkey); p++) {
		uint8_t hexh = 0xff, hexl = 0xff;
		ok = ok && a2rule_hexdigit (&hexh, *hexkey++);
		ok = ok && a2rule_hexdigit (&hexl, *hexkey++);
		dest [p] = (hexh << 4) | hexl;
	}
	return ok;
}

/* Open the database and locate the desired ruleset.
 * The ruleset will be cloned for SET and DEL opcodes.
 * For closing after success, use a2rule_dbclose().
 */
bool a2rule_dbopen (struct rules_db *rdb,
		opcode_t opcode, const struct cmdparser *prs,
		const rules_type rtp,
		const rules_domain dom,
		const char *nameptr, unsigned namelen,
		const a2sel_t *opt_sel,
		MDB_val *ruleset_found) {
	//
	// Storage for the 3 levels of database key
	rules_dbkey lookup1;
	rules_dbkey lookup2;
	rules_dbkey lookup3;
	//
	// Clear the database handle structure
	memset (rdb, 0, sizeof (*rdb));
	//
	// Parse the database trunk value, or use the default
	uint32_t trk = RULES_TRUNK_ANY;
	if (prs->VAL_DBTRUNK != NULL) {
		char *end = NULL;
		trk = (uint32_t) strtoul (prs->VAL_DBTRUNK, &end, 0);
		if ((end == NULL) || (*end != '\0')) {
			fprintf (stderr, "Failed to parse database trunk '%s'\n", prs->VAL_DBTRUNK);
			//TODO// errno = EINVAL;
			return false;
		}
	}
	//
	// Copy the Rules Name on stack with a trailing NUL
	char name [namelen + 1];
	memcpy (name, nameptr, namelen);
	name [namelen] = '\0';
	//
	// Make sure to have a Selector, setting the default "@." if need be
	a2sel_t def_sel;
	if (opt_sel == NULL) {
		assertxt (a2sel_parse (&def_sel, "@.", 2), "Internal error: default selector");
		opt_sel = &def_sel;
	}
	//
	// Take in the Database Secret, Domain Key or Service Key
	char *dbsecret = prs->VAL_DBSECRET;
	unsigned dbsecretlen = (dbsecret != NULL) ? strlen (dbsecret) : 0;
	char *domain_key = prs->VAL_DBDOMKEY;
	char *service_key = prs->VAL_DBSVCKEY;
	//
	// Fallback on ARPA2_DOMAINKEY_<dom> for the Domain Key
	if ((dbsecret == NULL) && (domain_key == NULL) && (service_key == NULL)) {
		/* UTF-8 domain names can be surprisingly long */
		/* Funny codes are mapped to an underscore */
		char varname [300];
		memcpy  (varname, "ARPA2_DOMAINKEY_", 16);
		char *varptr = varname + 16;
		char *domptr = dom;
		while (true) {
			char dc = *domptr++;
			/* Map characters, or ignore them */
			if (dc == '\0') {
				/* got the entire domain name */
				break;
			} else if (varptr == varname + 16 + 255) {
				/* ignore the remainder */
				break;
			} else if ((dc >= 'A') && (dc <= 'Z')) {
				/* map to lowercase */
				dc |= 0x20;
			} else if ((dc >= 'a') && (dc <= 'z')) {
				/* accept */ ;
			} else if ((dc >= '0') && (dc <= '9')) {
				/* accept */ ;
			} else if (dc == '.') {
				/* rewrite */
				dc = '_';
			} else if (dc == '-') {
				/* rewrite */
				dc = 'X';
			} else {
				/* ignore */
				continue;
			}
			*varptr++ = dc;
		}
		*varptr = '\0';
		domain_key = getenv (varname);
		if (domain_key == NULL) {
			fprintf (stderr, "Domain Key not in $%s\n", varname);
		}
	}
	//
	// Parse the Domain Key, if it is present
	if (domain_key != NULL) {
		if (!a2rule_parsekey (lookup1, domain_key)) {
			fprintf (stderr, "Provide %d hex characters as a Domain Key\n",
					2 * (int) sizeof (rules_dbkey));
			//TODO// errno = EINVAL;
			return false;
		}
	}
	//
	// Parse the Service Key, if it is present
	if (service_key != NULL) {
		memset (lookup1, 0, sizeof (rules_dbkey));
		if (!a2rule_parsekey (lookup2, service_key)) {
			fprintf (stderr, "Provide %d hex characters as a Service Key\n",
					2 * (int) sizeof (rules_dbkey));
			//TODO// errno = EINVAL;
			return false;
		}
	}
	//
	// Fallback on getpass () for the Database Secret
	if ((dbsecret == NULL) && (domain_key == NULL) && (service_key == NULL)) {
		dbsecret = getpass ("Database Secret: ");
		if ((dbsecret == NULL) || (*dbsecret == '\0')) {
			dbsecret    = NULL;
			dbsecretlen = 0;
		} else {
			dbsecretlen = strlen (dbsecret);
		}
	}
	//
	// Compute the database lookup keys in 3 levels
	bool ok = true;
	if ((domain_key == NULL) && (service_key == NULL)) {
		ok = ok && rules_dbkey_domain   (lookup1, dbsecret, dbsecretlen, dom);
		if (dbsecret != NULL) {
			memset (dbsecret, 0, dbsecretlen);
		}
	}
	if (service_key == NULL) {
		ok = ok && rules_dbkey_service  (lookup2, lookup1, sizeof (lookup1), rtp);
	}
	if (true) {
		ok = ok && rules_dbkey_selector (lookup3, lookup2, sizeof (lookup2), name, opt_sel);
	}
	if (!ok) {
		return false;
	}
	//
	// Print the trunk and key derivation process
	printf          ("Trunk number: %d\n", (int) trk);
	a2rule_printkey ("Domain   Key: ", lookup1);
	a2rule_printkey ("Service  Key: ", lookup2);
	a2rule_printkey ("Selector Key: ", lookup3);
	/* printf ("See also: http://internetwide.org/blog/2020/12/21/xs-1-dialing.html\n"); */
	//
	// Decide whether the database is to be readonly
	bool rdonly = (opcode > OPCODE_LAST_DBWRITER);
	//
	// Open the database
	if (!rules_dbopen (rdb, rdonly, trk)) {
		return false;
	}
	//
	// Decide whether data must be cloned
	bool cloner = ((opcode == OPCODE_SET) || (opcode == OPCODE_DEL));
	//
	// Look for the ruleset; not found is okay with empty data
	MDB_val orig;
	MDB_val *getdata = cloner ? &orig : ruleset_found;
	if (!rules_dbget (rdb, lookup3, getdata)) {
		/* There were operational problems with the database */
		rules_dbclose (rdb);
		return false;
	}
	//
	// Clone the ruleset to allow changes
	if (cloner) {
		ruleset_found->mv_size =         orig.mv_size ;
		ruleset_found->mv_data = malloc (orig.mv_size);
		if (ruleset_found->mv_data == NULL) {
			/* errno already set to ENOMEM */
			rules_dbclose (rdb);
			return false;
		}
		memcpy (ruleset_found->mv_data, orig.mv_data, orig.mv_size);
	}
	//
	// Return success
	return true;
}


/* Match the a2rule_dbopen() call.  There is an implied buffer allocation
 * in that call, at least for write-opens, cleanup now.
 */
void a2rule_dbclose (struct rules_db *rdb,
		opcode_t opcode,
		MDB_val *ruleset_found) {
	//
	// Decide whether data must be cloned
	bool cloner = ((opcode == OPCODE_SET) || (opcode == OPCODE_DEL));
	//
	// Cleanup the allocated source
	if (cloner) {
		free (ruleset_found->mv_data);
		ruleset_found->mv_data = NULL;
		ruleset_found->mv_size = 0;
	}
	//
	// Free the database
	rules_dbclose (rdb);
}


/* Default type for match callbacks.  Which fields are used is
 * up to the RuleDB application.
 */
typedef struct cbrulematchdata {
	const char *rule;
	struct cmdparser *prs;
	a2id_t *local;
	a2id_t *remote;
} cbrulematchdata_t;


/* Callback routine for rule handling.
 *
 * @param cbdata Opaque pointer to callback data
 * @returns True to indicate that a match was found, and
 *	the requested @a opcode_t can be performed.
 *	Note that merely listing rules in a ruleset may
 *	never report true.
 */
typedef bool cb_rule_t (cbrulematchdata_t *cbdata, char *rule);


/* Loop over the entries in a previously loaded ruleset.
 * Each rule triggers a callback.  The number of callbacks
 * that return true is counted and returned.  When a callback
 * sets the @a cuthere flag then the current rule is removed.
 *
 * @param opcode Whether to ADD, SET, DEL or GET rules
 * @param ruleset The ruleset to iterate over; may be changed
 * @param cbfun The callback function, see cb_rule_t
 * @param cbdata Opaque pointer to callback data
 * @returns Whether the number of rule matches agreed with @a opcode
 */
bool a2rule_iter (opcode_t opcode, MDB_val *ruleset, cb_rule_t cbfun, cbrulematchdata_t *cbdata) {
	unsigned rulectr = 0;
	char *rule;
	bool cuthere;
	//
	// Start iteration, skipping if empty
	if (rules_dbloop (ruleset, &rule)) do {
		cuthere = false;
		//
		// Test if this rule was set by us
		unsigned dbprefixlen = strlen (cbdata->prs->VAL_DBPREFIX);
		if ((strncmp (rule, cbdata->prs->VAL_DBPREFIX, dbprefixlen) != 0) || (rule [dbprefixlen] != ' ')) {
			continue;
		}
		//
		// See if the rule matches
		bool match = cbfun (cbdata, rule);
		if (match) {
			//
			// Count the number of matched rules
			rulectr++;
			//
			// Proceed according to the type of call
			switch (opcode) {
			case OPCODE_DEL:
			case OPCODE_SET:
				log_debug ("Removing rule '%s'\n", rule);
				cuthere = match;
				break;
			case OPCODE_ADD:
				fprintf (stderr, "Already have a rule '%s'\n", rule);
				break;
			case OPCODE_GET:
				printf ("DBrule: %s\n", rule);
				break;
			default:
				break;
			}
		}
		//
		// Continue iteration, after possibly cutting out the current rule
	} while (rules_dbcutnext (ruleset, &rule, cuthere));
	//
	// Return if the overall iteration worked
	switch (opcode) {
	case OPCODE_DEL:
	case OPCODE_GET:
		return (rulectr > 0);
	case OPCODE_ADD:
		return (rulectr == 0);
	case OPCODE_SET:
		return true;
	default:
		return false;
	}
}



/***** Helper routines *****/



/* Check RIGHTS syntax.  Map a LIST keyword to a RIGHTS value.
 * Return true on success, false on syntax error.
 */
bool comm_list2rights (struct cmdparser *prs) {
	bool ok = true;
	if (prs->VAL_LIST == NULL) {
		ok = ok && ( prs->VAL_RIGHTS != NULL  );
		ok = ok && (*prs->VAL_RIGHTS != '\x00');
		for (char *rp = prs->VAL_RIGHTS; ok && (*rp != '\x00'); rp++) {
			ok = ok && (*rp >= 'A') && (*rp <= 'Z');
		}
	} else {
		ok = ok && (prs->VAL_RIGHTS == NULL);
		static const char *kv [] = {
			"white",    "W",
			"grey",     "C",
			"gray",     "C",
			"black",    "V",
			"honeypot", "K",
			NULL
		};
		if (ok) {
			ok = false;
			for (const char **kvp = kv; *kvp != NULL; kvp += 2) {
				if (0 == strcmp (*kvp, prs->VAL_LIST)) {
					prs->VAL_RIGHTS = (char *) kvp [1];
					ok = true;
				}
			}
		}
	}
	return ok;
}


/* Find the given attribute in a rule.  When multiple entries
 * exist, use the last one.  (This is a slight generalisation
 * of Rule logic, but our own rules do not use this complexity.)
 *
 * Return NULL when not found, or when the last entry is empty.
 *
 * Use strchrnul(_, ' ') to skip over the returned attrvalue.
 */
const char *a2rule_attr (const char *rule, char attr) {
	const char *retval = NULL;
	//
	// Iterate over space-separated words in the rule
	while (rule = strchrnul (rule + 1, ' '), *rule != '\0') {
		//
		// Skip when this is not an attribute
		if (rule [1] != '=') {
			continue;
		}
		//
		// Skip when this is not the right attribute
		if (rule [2] != attr) {
			continue;
		}
		//
		// Set the return value, use NULL if empty
		if (rule [3] == '\0') {
			retval = NULL;
		} else {
			retval = rule + 3;
		}
	}
	//
	// Return the last attribute value found
	return retval;
}



/***** Subcommand routines *****/



/* Ruleset match routine.
 *
 * This matches an accurate rule when given, or anything
 * if no rule is given.
 */
bool match_ruleset (cbrulematchdata_t *cbdata, char *rule) {
	if (cbdata->rule == NULL) {
		return true;
	}
	return (strcmp (cbdata->rule, rule) == 0);
}


/* Communication match routine.
 *
 * This matches "input" attributes =a<ALIAS|ENDALIAS>[@],
 * =s<SIGNFLAGS>.  These are part of rule selection in
 * "comm get|del" and are setup by "comm add|set", where
 * "comm set" first removes rules with these attributes.
 *
 * "Output" attributes =o<OVRALI> =n<NEWUSERID> =g<ACTORID> are
 * ignored.  The output %RIGHTS are also ignored.  Such are
 * always subject to SETting.
 *
 * The other elements of communication have been confirmed via
 * database lookup; the userid and domain from local identity
 * and the full remote selector have been incorporated.
 */
bool match_comm (cbrulematchdata_t *cbdata, char *rule) {
	log_debug ("Matching local '%s' with rule '%s'", cbdata->local->txt, rule);
	//
	// Lookup the alias and signature flags in the rule
	const char *rule_sigfl = a2rule_attr (rule, 's');
	const char *rule_alias = a2rule_attr (rule, 'a');
	//
	// Compute the numeric value for the signature flags
	uint32_t rule_sigfl_int = (rule_sigfl != NULL) ? atoi (rule_sigfl) : 0;
	//
	// Do not match if the local signature flags are too lenient
	if ((rule_sigfl_int & ~cbdata->local->sigflags) != 0) {
		log_debug ("Rule sigflags %x rule out local sigflags %x", rule_sigfl_int, cbdata->local->sigflags);
		return false;
	}
	//
	// Do not match if the local signature has expired
	//TODO:EXPIRATION//
	//
	// Find the rule alias length, reduce by one for '@' ending
	unsigned rule_alias_len = 0;
	bool rule_alias_end = false;
	if (rule_alias != NULL) {
		rule_alias_len = strchrnul (rule_alias, ' ') - rule_alias;
		if (rule_alias [rule_alias_len - 1] == '@') {
			rule_alias_end = true;
			rule_alias_len--;
		}
	}
	//
	// Work out the alias/svcargs and signflags in the localid
	const char *local_alias;
	unsigned    local_alias_len;
	bool        local_alias_end = false;
	if (cbdata->prs->VAL_ENDALIAS != NULL) {
		local_alias_end = true;
		local_alias = cbdata->prs->VAL_ENDALIAS;
	} else if (cbdata->prs->VAL_ALIAS != NULL) {
		local_alias = cbdata->prs->VAL_ALIAS;
	} else {
		local_alias = "";
	}
	local_alias_len = strlen (local_alias);
	//
	// Do not match if rule_alias_end and local_alias_end differ
	if (rule_alias_end != local_alias_end) {
		log_debug ("Local alias ending %d differs from rule alias ending %d", local_alias_end, rule_alias_end);
		return false;
	}
	//
	// Do not match if the rule prescribes a longer alias
	if (local_alias_len < rule_alias_len) {
		log_debug ("Local alias length %d less than rule alias length %d", local_alias_len, rule_alias_len);
		return false;
	}
	//
	// Do not match if the rule alias length does not match
	if (strncmp (local_alias, rule_alias, rule_alias_len) != 0) {
		log_debug ("Local alias '%.*s' does not start like rule alias '%.*s'", local_alias_len, local_alias, rule_alias_len, rule_alias);
		return false;
	}
	//
	// Consider longer local alias length than rule alias length
	if (local_alias_len > rule_alias_len) {
		//
		// Do not match if the continuation does not start with '+'
		if (local_alias [rule_alias_len] != '+') {
			log_debug ("Local alias does not extend after rule with a '+'");
			return false;
		}
		//
		// Do not match if the continuation is forbidden
		if (rule_alias_end) {
			log_debug ("Rule does not permit extra aliases");
			return false;
		}
	}
	return true;
}


/* Document match routine.
 *
 * This matches "input" attributes, but none of these are
 * defined for Document Access as part of rule selection in
 * "docu get|del" and are setup by "docu add|set", where
 * "docu set" first removes rules with these attributes.
 *
 * "Output" attributes =g<ACTORID> are ignored.  The output
 * %RIGHTS are also ignored.  Such are always subject to SETting.
 *
 * The other elements of documents have been confirmed via
 * database lookup; the domain, the colloction or volume with
 * optional path, as well as the full full remote selector have
 * been incorporated.
 */
bool match_docu (cbrulematchdata_t *cbdata, char *rule) {
	return true;
}


/* Group matching routine.
 *
 * The database lookup key matches the domain and group name.
 *
 * Always required is the "input" data of member alias, which occurs
 * in the ^member@deliveryaddress trigger.
 *
 * Not matched are the "output" data parts, namely the %MARKS and
 * the deliveryaddress.  They are simply set along with the rest.
 */
bool match_group (cbrulematchdata_t *cbdata, char *rule) {
	log_debug ("Matching member '%s' with rule '%s'", cbdata->prs->VAL_MEMBER, rule);
	//
	// Find the trigger ^member@deliveryaddress
	char *rule_member = strchrnul (rule, '^');
	if (*rule_member == '\x00') {
		log_error ("No trigger in group rule '%s'", rule);
		return false;
	}
	rule_member++;
	unsigned rule_member_len = strchrnul (rule_member, '@') - rule_member;
	//
	// Find the member in the local ARPA2 Identity
	char *local_member = cbdata->local->txt + cbdata->local->ofs [A2ID_OFS_ALIASES];
	unsigned local_member_len = cbdata->local->ofs [A2ID_OFS_PLUS_SIG] - cbdata->local->ofs [A2ID_OFS_ALIASES];
	//
	// Do not accept when the member has no length
	if (local_member_len == 0) {
		log_debug ("No member alias in '%s'", cbdata->local->txt);
		return false;
	}
	//
	// Do not accept when members differ
	if ((local_member_len != rule_member_len) || (strncmp (local_member, rule_member, rule_member_len) != 0)) {
		log_debug ("Member alias differs");
		return false;
	}
	//
	// Return a match
	return true;
}


/* Ruleset subcommands: ruleset add|set|del|get
 */
bool ruleset_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	bool ok = true;
	//
	// If no DBPREFIX is given, use the default
	if (prs->VAL_DBPREFIX == NULL) {
		prs->VAL_DBPREFIX = "#a2xs";
	}
	//
	// Parse the SELECTOR, if present
	a2sel_t sel;
	if (prs->VAL_SELECTOR == NULL) {
		prs->VAL_SELECTOR = "@.";
	}
	if (!a2sel_parse (&sel, prs->VAL_SELECTOR, strlen (prs->VAL_SELECTOR))) {
		fprintf (stderr, "Failed to parse ARPA2 Selector '%s'\n", prs->VAL_SELECTOR);
		return false;
	}
	//
	// Recognise the TYPE, if present
	unsigned typelen = (prs->VAL_TYPE == NULL) ? 0 : strlen (prs->VAL_TYPE);
	char *typerex;
	char *svckey_envvar = "ARPA2_SERVICEKEY";
	if (typelen == 0) {
		typerex = "^.*$";
		//NO// svckey_envvar = "ARPA2_SERVICEKEY";
	} else if (strncasecmp (prs->VAL_TYPE, "communicate", typelen) == 0) {
		//TODO// Accuracy
		typerex = "^.*$";
		//NO// svckey_envvar = "ARPA2_SERVICEKEY_communicate";
	} else if (strncasecmp (prs->VAL_TYPE, "document", typelen) == 0) {
		//TODO// Accuracy
		typerex = "^/^[0-9a-fA-F-]+$/|//[a-zA-Z0-9_@]+/.*$";
		//NO// svckey_envvar = "ARPA2_SERVICEKEY_document";
	} else if (strncasecmp (prs->VAL_TYPE, "group", typelen) == 0) {
		//TODO// Accuracy
		typerex = "^.*$";
		//NO// svckey_envvar = "ARPA2_SERVICEKEY_group";
	} else {
		fprintf (stderr, "Only known TYPE values are COMMUNICATE, DOCUMENT, GROUP\n");
		return false;
	}
	//
	// Parse the NAME as indicate by the TYPE
	regex_t typechk;
	assertxt (0 == regcomp (&typechk, typerex, REG_EXTENDED),
		"Regular expression did not compile: %s\n", typerex);
	ok = ok && (regexec (&typechk, prs->VAL_NAME, 0, NULL, 0) == 0);
	//TODO// Replace ..., 0, NULL,... and print matched fields with their name
	regfree (&typechk);
	if (!ok) {
		fprintf (stderr, "Type %s regex %s failed on name %s\n", prs->VAL_TYPE, typerex, prs->VAL_NAME);
		return false;
	}
	//
	// Parse the Service Key, or get it from the environment
	if (prs->VAL_DBSVCKEY == NULL) {
		prs->VAL_DBSVCKEY = getenv (svckey_envvar);
		if (prs->VAL_DBSVCKEY == NULL) {
			fprintf (stderr, "Service Key not in $%s\n", svckey_envvar);
			return false;
		}
	}
	//
	// Since we have the Service Key, we need no Access Domain or Type
	char *no_domain = "";
	rules_type no_rules_type;
	memset (&no_rules_type, 0, sizeof (no_rules_type));
	//
	// Parse the RULE, if present
	if (prs->VAL_RULE != NULL) {
		struct rules_request req;
		memset (&req, 0, sizeof (req));
		unsigned rulesetlen = strlen (prs->VAL_RULE) + 1;
		bool permit_selector = false;
		if (!rules_process (&req, prs->VAL_RULE, rulesetlen, permit_selector)) {
			//TODO// Benefit from com_err code
			fprintf (stderr, "Rule syntax is wrong\n");
			return false;
		}
	}
	//
	// Open the database and find the ruleset
	struct rules_db ruledb;
	MDB_val olddata;
	if (!a2rule_dbopen (&ruledb, opcode,
			prs,                    /* Trunk ID,
			                           Database Secret */
			no_rules_type,          /* Access Type */
			no_domain,              /* Access Domain */
			prs->VAL_NAME, strlen (prs->VAL_NAME),
			                        /* Access Name */
			&sel,                   /* Remote Selector */
			&olddata)) {
		fprintf (stderr, "Failure accessing the database\n");
		goto fail;
	}
	//
	// Look for pre-existing rule; cut for set; complain for add
	cbrulematchdata_t rmd = {
		.rule = prs->VAL_RULE,
		.prs = prs,
		.local = NULL,
		.remote = &sel,
	};
log_debug ("Before iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	ok = ok && a2rule_iter (opcode, &olddata, match_ruleset, &rmd);
log_debug ("After  iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	if (!ok) {
		char *msg;
		if (opcode <= OPCODE_LAST_CREATOR) {
			msg = "Refusing to break database integrity";
		} else {
			msg = "No rules found in the database";
		}
		fprintf (stderr, "%s\n", msg);
		goto closefail;
	}
	//
	// Add the new rule for "ruleset add|set"
	// Plus, separate handling for "ruleset del"
	if ((opcode == OPCODE_ADD) || (opcode == OPCODE_SET)) {
		//
		// Write to the database location
		MDB_val newdata = {
			.mv_data =         prs->VAL_RULE     ,
			.mv_size = strlen (prs->VAL_RULE) + 1,
		};
		log_data ("Old data", olddata.mv_data, olddata.mv_size, 0);
		log_data ("New data", newdata.mv_data, newdata.mv_size, 0);
		if (!rules_dbset (&ruledb, &olddata, &newdata)) {
			fprintf (stderr, "Failed to add|set '%.*s' to ruleset of size #%d\n", (int) newdata.mv_size, (char *) newdata.mv_data, (int) olddata.mv_size);
			goto closefail;
		}
	} else if (opcode == OPCODE_DEL) {
		//
		// Setup an empty addition
		MDB_val nodata = {
			.mv_data = "",
			.mv_size = 0
		};
		if (!rules_dbset (&ruledb, &olddata, &nodata)) {
			fprintf (stderr, "Failed to del rules\n");
			goto closefail;
		}
	}
	//
	// Commit any changes for "comm add|set|del"
	if (opcode <= OPCODE_LAST_DBWRITER) {
		ok = ok && rules_dbcommit (&ruledb);
		if (!ok) {
			fprintf (stderr, "The database was not changed\n");
			goto fail;
		}
	}
	//
	// Close ACLdb
	a2rule_dbclose (&ruledb, opcode, &olddata);
	//
	// Return overall result
	return ok;
	//
	// Close ACLdb and return failure
closefail:
	a2rule_dbclose (&ruledb, opcode, &olddata);
fail:
	return false;
}



/* Communication subcommands: comm add|set|del|get
 */
bool comm_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	bool ok = true;
	//
	// If no DBPREFIX is given, use the default
	if (prs->VAL_DBPREFIX == NULL) {
		prs->VAL_DBPREFIX = "#a2xs";
	}
	//
	// Parse argv[]
	if (opcode <= OPCODE_LAST_CREATOR) {
		ok = ok && comm_list2rights (prs);
	}
	if (!ok) {
		cmdparse_usage (usage, 2, argv);
		goto fail;
	}
	//
	// Parse the local and remote identities
	a2id_t  lid;
	a2sel_t rid;
	if (!a2id_parse  (&lid, prs->VAL_LOCAL , strlen (prs->VAL_LOCAL ))) {
		fprintf (stderr, "Failed to parse local ARPA2 Identity '%s'\n", prs->VAL_LOCAL);
		goto fail;
	}
	if (lid.ofs [A2ID_OFS_PLUS_SVCARGS] != lid.ofs [A2ID_OFS_USERID]) {
		fprintf (stderr, "The local ARPA2 Identity must not carry service arguments\n");
		goto fail;
	}
	if (lid.ofs [A2ID_OFS_PLUS_ALIASES] != lid.ofs [A2ID_OFS_PLUS_SIG]) {
		fprintf (stderr, "The local ARPA2 Identity must not carry aliases\n");
		goto fail;
	}
	if (lid.ofs [A2ID_OFS_PLUS_SIG] != lid.ofs [A2ID_OFS_AT_DOMAIN]) {
		fprintf (stderr, "The local ARPA2 Identity must not carry signature data\n");
		goto fail;
	}
	if (!a2sel_parse (&rid, prs->VAL_REMOTE, strlen (prs->VAL_REMOTE))) {
		fprintf (stderr, "Failed to parse remote ARPA2 Selector '%s'\n", prs->VAL_REMOTE);
		goto fail;
	}
	//
	// The Access Name is different for user and service
	char     *ltxt = lid.txt;
	uint16_t *lofs = lid.ofs;
	char *domain = ltxt + lofs [A2ID_OFS_DOMAIN];
	char    *nameptr;
	unsigned namelen;
	if (ltxt [lofs [A2ID_OFS_PLUS_SERVICE]] == '+') {
		nameptr = ltxt                         + lofs [A2ID_OFS_PLUS_SERVICE];
		namelen = lofs [A2ID_OFS_PLUS_SVCARGS] - lofs [A2ID_OFS_PLUS_SERVICE];
	} else {
		nameptr = ltxt                         + lofs [A2ID_OFS_USERID];
		namelen = lofs [A2ID_OFS_PLUS_ALIASES] - lofs [A2ID_OFS_USERID];
	}
	//
	// Open the database and find the ruleset
	struct rules_db ruledb;
	MDB_val olddata;
	if (!a2rule_dbopen (&ruledb, opcode,
			prs,                    /* Trunk ID,
			                           Database Secret */
			access_type_comm,       /* Access Type */
			domain,                 /* Access Domain */
			nameptr, namelen,       /* Access Name */
			&rid,                   /* Remote Selector */
			&olddata)) {
		fprintf (stderr, "Failure accessing the database\n");
		goto fail;
	}
	//
	// Fill a buffer with the newly added rule
	ssize_t newrulelen = ok ? 0 : -1;
	char newrule [1024];
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			"%s", prs->VAL_DBPREFIX);
	if ((prs->VAL_ALIAS != NULL) || (prs->VAL_ENDALIAS != NULL)) {
		//
		// Optionally insert " =a<ALIAS|ENDALIAS><@-for-<ENDALIAS>>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =a%s%s%s",
			(prs->VAL_ALIAS    != NULL) ? prs->VAL_ALIAS    : "",
			(prs->VAL_ENDALIAS != NULL) ? prs->VAL_ENDALIAS : "",
			(prs->VAL_ENDALIAS != NULL) ? "@" : "");
	}
	if (prs->VAL_SIGNFLAGS != NULL) {
		//
		// Optionally insert " =s<SIGNFLAGS>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =s%s",
			prs->VAL_SIGNFLAGS);
	}
	if (prs->VAL_NEWUSERID != NULL) {
		//
		// Optionally insert "=n<NEWUSERID>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =n%s",
			prs->VAL_NEWUSERID);
	}
	if (prs->VAL_NEWALIAS != NULL) {
		//
		// Optionally insert " =o<OVRALI>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =o%s",
			prs->VAL_NEWALIAS);
	}
	if (prs->VAL_ACTORID != NULL) {
		//
		// Optionally insert " =g<ACTORID>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =g%s",
			prs->VAL_ACTORID);
	}
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" %%%s", prs->VAL_RIGHTS);
	if (((unsigned long) newrulelen++) >= sizeof (newrule)) {
		fprintf (stderr, "Rule size %lu seems excessive\n", newrulelen);
		goto closefail;
	}
	//
	// Look for pre-existing rule; cut for set; complain for add
	cbrulematchdata_t rmd = {
		.rule = newrule,
		.prs = prs,
		.local = &lid,
		.remote = &rid,
	};
log_debug ("Before iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	ok = ok && a2rule_iter (opcode, &olddata, match_comm, &rmd);
log_debug ("After  iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	if (!ok) {
		char *msg;
		if (opcode <= OPCODE_LAST_CREATOR) {
			msg = "Refusing to break database integrity";
		} else {
			msg = "No rules found in the database";
		}
		fprintf (stderr, "%s\n", msg);
		goto closefail;
	}
	//
	// Add the new rule for "comm add|set"
	// Plus, separate handling for "comm del"
	if ((opcode == OPCODE_ADD) || (opcode == OPCODE_SET)) {
		//
		// Write to the database location
		MDB_val newdata = {
			.mv_data = newrule,
			.mv_size = newrulelen,
		};
		log_data ("Old data", olddata.mv_data, olddata.mv_size, 0);
		log_data ("New data", newdata.mv_data, newdata.mv_size, 0);
		if (!rules_dbset (&ruledb, &olddata, &newdata)) {
			fprintf (stderr, "Failed to add|set '%.*s' to ruleset of size #%d\n", (int) newdata.mv_size, (char *) newdata.mv_data, (int) olddata.mv_size);
			goto closefail;
		}
	} else if (opcode == OPCODE_DEL) {
		//
		// Setup an empty addition
		MDB_val nodata = {
			.mv_data = "",
			.mv_size = 0
		};
		if (!rules_dbset (&ruledb, &olddata, &nodata)) {
			fprintf (stderr, "Failed to del rules\n");
			goto closefail;
		}
	}
	//
	// Commit any changes for "comm add|set|del"
	if (opcode <= OPCODE_LAST_DBWRITER) {
		ok = ok && rules_dbcommit (&ruledb);
		if (!ok) {
			fprintf (stderr, "The database was not changed\n");
			goto fail;
		}
	}
	//
	// Close ACLdb
	a2rule_dbclose (&ruledb, opcode, &olddata);
	//
	// Return overall result
	return ok;
	//
	// Close ACLdb and return failure
closefail:
	a2rule_dbclose (&ruledb, opcode, &olddata);
fail:
	return false;
}


/* Document subcommands: docu add|set|del|get
 */
bool docu_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	bool ok = true;
	//
	// If no DBPREFIX is given, use the default
	if (prs->VAL_DBPREFIX == NULL) {
		prs->VAL_DBPREFIX = "#a2xs";
	}
	//
	// Parse argv[]
#if 0
	if (opcode <= OPCODE_LAST_CREATOR) {
		ok = ok && comm_list2rights (prs);
	}
	if (!ok) {
		cmdparse_usage (usage, 2, argv);
		return false;
	}
#endif
	//
	// The Access Name is for a collection or a volume
	unsigned namelen = 10;
	if (prs->VAL_COLLECTION != NULL) {
		//TODO// PROPERLY SCAN UUID GRAMMAR
		if (strlen (prs->VAL_COLLECTION) != 36) {
			fprintf (stderr, "The collection must follow UUID syntax\n");
			return false;
		}
		//
		// Map uppercase hex to lowercase
		for (char *hp = prs->VAL_COLLECTION; *hp; hp++) {
			if ((*hp >= 'A') && (*hp <= 'F')) {
				*hp |= 0x20;
			}
		}
		//
		// Count the textual lenght of the UUID
		namelen += 36;
	} else {
		//
		// Count the volume name
		namelen += strlen (prs->VAL_VOLUME);
		//
		// Count the optional path
		if (prs->VAL_PATH != NULL) {
			namelen += strlen (prs->VAL_PATH);
		}
	}
	//
	// Allocate a buffer for the name (namelen is oversized)
	char namebuf [namelen+1];
	//
	// Copy the name into the buffer
	if (prs->VAL_COLLECTION != NULL) {
		//
		// The form is "/<colluuid>/"
		namelen = 38;
		namebuf [0] = '/';
		memcpy (namebuf + 1, prs->VAL_COLLECTION, namelen-2);
		namebuf [namelen + 1 - 2] = '/';
		namebuf [namelen + 1 - 1] = '\0';
	} else {
		//
		// Strip initial '/' from the <path>
		if (prs->VAL_PATH != NULL) {
			while (*prs->VAL_PATH == '/') {
				prs->VAL_PATH++;
			}
			if (*prs->VAL_PATH == '\0') {
				prs->VAL_PATH = NULL;
			}
		}
		//
		// The form is "//<volume>/<path>"
		namelen = sprintf (namebuf, "//%s/%s",
			                            prs->VAL_VOLUME     ,
			(prs->VAL_PATH   != NULL) ? prs->VAL_PATH   : "");
	}
	log_debug ("Document Access Name is \"%s\" #%d", namebuf, namelen);
	//
	// Parse the domain and remote identities
	char *domain = prs->VAL_LOCAL;
	if (strchr (domain, '.') == NULL) {
		fprintf (stderr, "Use a fully qualified domain name\n");
		goto fail;
	}
	for (char *dp = domain; *dp; dp++) {
		if ((*dp >= 'A') && (*dp <= 'Z')) {
			*dp |= 0x20;
		}
	}
	a2sel_t rid;
	if (!a2sel_parse (&rid, prs->VAL_REMOTE, strlen (prs->VAL_REMOTE))) {
		fprintf (stderr, "Failed to parse remote ARPA2 Selector '%s'\n", prs->VAL_REMOTE);
		goto fail;
	}
	//
	// Open the database and find the ruleset
	struct rules_db ruledb;
	MDB_val olddata;
	if (!a2rule_dbopen (&ruledb, opcode,
			prs,                    /* Trunk ID,
			                           Database Secret */
			access_type_document,   /* Access Type */
			domain,                 /* Access Domain */
			namebuf, namelen,       /* Access Name */
			&rid,                   /* Remote Selector */
			&olddata)) {
		fprintf (stderr, "Failure accessing the database\n");
		goto fail;
	}
	//
	// Fill a buffer with the newly added rule
	ssize_t newrulelen = ok ? 0 : -1;
	char newrule [1024];
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			"%s", prs->VAL_DBPREFIX);
	if (prs->VAL_ACTORID != NULL) {
		//
		// Optionally insert " =g<ACTORID>@<DOMAIN>"
		newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" =g%s@%s",
			prs->VAL_ACTORID,
			prs->VAL_DOMAIN);
	}
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" %%%s", prs->VAL_RIGHTS);
	if (((unsigned long) newrulelen++) >= sizeof (newrule)) {
		fprintf (stderr, "Rule size %lu seems excessive\n", newrulelen);
		goto closefail;
	}
	//
	// Look for pre-existing rule; cut for set; complain for add
	cbrulematchdata_t rmd = {
		.rule = newrule,
		.prs = prs,
		.local = NULL,
		.remote = &rid,
	};
log_debug ("Before iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	ok = ok && a2rule_iter (opcode, &olddata, match_docu, &rmd);
log_debug ("After  iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	if (!ok) {
		char *msg;
		if (opcode <= OPCODE_LAST_CREATOR) {
			msg = "Refusing to break database integrity";
		} else {
			msg = "No rules found in the database";
		}
		fprintf (stderr, "%s\n", msg);
		goto closefail;
	}
	//
	// Add the new rule for "comm add|set"
	// Plus, separate handling for "comm del"
	if ((opcode == OPCODE_ADD) || (opcode == OPCODE_SET)) {
		//
		// Write to the database location
		MDB_val newdata = {
			.mv_data = newrule,
			.mv_size = newrulelen,
		};
		log_data ("Old data", olddata.mv_data, olddata.mv_size, 0);
		log_data ("New data", newdata.mv_data, newdata.mv_size, 0);
		if (!rules_dbset (&ruledb, &olddata, &newdata)) {
			fprintf (stderr, "Failed to add|set '%.*s' to ruleset of size #%d\n", (int) newdata.mv_size, (char *) newdata.mv_data, (int) olddata.mv_size);
			goto closefail;
		}
	} else if (opcode == OPCODE_DEL) {
		//
		// Setup an empty addition
		MDB_val nodata = {
			.mv_data = "",
			.mv_size = 0
		};
		if (!rules_dbset (&ruledb, &olddata, &nodata)) {
			fprintf (stderr, "Failed to del rules\n");
			goto closefail;
		}
	}
	//
	// Commit any changes for "comm add|set|del"
	if (opcode <= OPCODE_LAST_DBWRITER) {
		ok = ok && rules_dbcommit (&ruledb);
		if (!ok) {
			fprintf (stderr, "The database was not changed\n");
			goto fail;
		}
	}
	//
	// Close ACLdb
	a2rule_dbclose (&ruledb, opcode, &olddata);
	//
	// Return overall result
	return ok;
	//
	// Close ACLdb and return failure
closefail:
	a2rule_dbclose (&ruledb, opcode, &olddata);
fail:
	return false;
}


/* Group subcommands: group add|set|del|get
 */
bool group_ops (opcode_t opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []) {
	bool ok = true;
	//
	// If no DBPREFIX is given, use the default
	if (prs->VAL_DBPREFIX == NULL) {
		prs->VAL_DBPREFIX = "#a2xs";
	}
	//
	// Have a fallback selector
	a2sel_t fallback_selector;
	assertxt (a2sel_parse (&fallback_selector, "@.", 2), "Internal error: fallback_selector");
	//
	// Parse the local and remote identities
	a2act_t member;
	a2id_t identity;
	ok = ok && a2act_parse (&member, prs->VAL_MEMBER, strlen (prs->VAL_MEMBER), 1);
	if (!ok) {
		fprintf (stderr, "Failed to parse member '%s'\n", prs->VAL_MEMBER);
		goto fail;
	}
	if (prs->VAL_IDENTITY != NULL) {
		ok = ok && a2id_parse_remote (&identity, prs->VAL_IDENTITY, strlen (prs->VAL_IDENTITY));
	} else {
		memset (&identity, 0, sizeof (identity));
	}
	if (!ok) {
		fprintf (stderr, "Failed to parse delivery address '%s'\n", prs->VAL_IDENTITY);
		goto fail;
	}
	printf ("ok=%d at %s:%d\n", ok, __FILE__, __LINE__);
	//
	// The Access Name is the <group> part before the +<member>@<domain>
	char    *nameptr = member.txt;
	unsigned namelen = member.ofs [A2ID_OFS_PLUS_ALIASES];
	//
	// The Member Name is the <member> part after <group>+ and before @<domain>
	char    *memberptr = member.txt                     + member.ofs [A2ID_OFS_ALIASES];
	unsigned memberlen = member.ofs [A2ID_OFS_PLUS_SIG] - member.ofs [A2ID_OFS_ALIASES];
	char *domain = member.txt + member.ofs [A2ID_OFS_DOMAIN];
	//
	// Open ACLdb, using dbtrunk, dbsecret, dbdomkey as needed
	struct rules_db ruledb;
	MDB_val olddata;
	if (!a2rule_dbopen (&ruledb, opcode,
			prs,                    /* Trunk ID,
			                           Database Secret */
			rules_type_group,       /* Access Type */
			domain,                 /* Access Domain */
			nameptr, namelen,       /* Access Name */
			&fallback_selector,     /* No Selector */
			&olddata)) {
		fprintf (stderr, "Failure accessing the database\n");
		goto fail;
	}
	//
	// Fill a buffer with the newly added rule
	ssize_t newrulelen = ok ? 0 : -1;
	char newrule [1024];
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			"%s", prs->VAL_DBPREFIX);
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" %%%s", prs->VAL_MARKS);
	newrulelen = snoprintf (newrule, sizeof (newrule), newrulelen,
			" ^%.*s@%s",
				// "member@..."
				memberlen, memberptr,
				// "...@identity"
				identity.txt);
	if (((unsigned long) newrulelen++) >= sizeof (newrule)) {
		fprintf (stderr, "Rule size %lu seems excessive\n", newrulelen);
		goto closefail;
	}
	//
	// Look for pre-existing rule; cut for set; complain for add
	cbrulematchdata_t rmd = {
		.rule = newrule,
		.prs = prs,
		.local = &member,
		.remote = &identity,
	};
log_debug ("Before iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	ok = ok && a2rule_iter (opcode, &olddata, match_group, &rmd);
log_debug ("After  iteration, ruleset takes %d bytes and ok=%d", (int) olddata.mv_size, ok);
	if (!ok) {
		char *msg;
		if (opcode <= OPCODE_LAST_CREATOR) {
			msg = "Refusing to break database integrity";
		} else {
			msg = "No rules found in the database";
		}
		fprintf (stderr, "%s\n", msg);
		goto closefail;
	}
	//
	// Add the new rule for "comm add|set"
	// Plus, separate handling for "comm del"
	if ((opcode == OPCODE_ADD) || (opcode == OPCODE_SET)) {
		//
		// Write to the database location
		MDB_val newdata = {
			.mv_data = newrule,
			.mv_size = newrulelen,
		};
		log_data ("New data", newdata.mv_data, newdata.mv_size, 0);
		log_data ("Old data", olddata.mv_data, olddata.mv_size, 0);
		ok = ok && rules_dbset (&ruledb, &olddata, &newdata);
	} else if (opcode == OPCODE_DEL) {
		//
		// Setup an empty addition
		MDB_val nodata = {
			.mv_data = "",
			.mv_size = 0
		};
		if (!rules_dbset (&ruledb, &olddata, &nodata)) {
			fprintf (stderr, "Failed to del rules\n");
			goto closefail;
		}
	}
	//
	// Commit any changes for "group add|set|del"
	if (opcode <= OPCODE_LAST_DBWRITER) {
		ok = ok && rules_dbcommit (&ruledb);
		if (!ok) {
			fprintf (stderr, "The database was not changed\n");
			goto fail;
		}
	}
	//
	// Close ACLdb
	a2rule_dbclose (&ruledb, opcode, &olddata);
	//
	// Return overall result
	return ok;
	//
	// Close ACLdb and return failure
closefail:
	a2rule_dbclose (&ruledb, opcode, &olddata);
fail:
	return false;
}


/***** Main program *****/



static const struct cmdparse_action ruleset_syntax [] = {
	{ "add", OPCODE_ADD, &ruleset_addset_grammar, ruleset_addset_usage, ruleset_ops },
	{ "set", OPCODE_SET, &ruleset_addset_grammar, ruleset_addset_usage, ruleset_ops },
	{ "del", OPCODE_DEL, &ruleset_delget_grammar, ruleset_delget_usage, ruleset_ops },
	{ "get", OPCODE_GET, &ruleset_delget_grammar, ruleset_delget_usage, ruleset_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

static const struct cmdparse_action comm_syntax [] = {
	{ "add", OPCODE_ADD, &comm_addset_grammar, comm_addset_usage, comm_ops },
	{ "set", OPCODE_SET, &comm_addset_grammar, comm_addset_usage, comm_ops },
	{ "del", OPCODE_DEL, &comm_delget_grammar, comm_delget_usage, comm_ops },
	{ "get", OPCODE_GET, &comm_delget_grammar, comm_delget_usage, comm_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

static const struct cmdparse_action docu_syntax [] = {
	{ "add", OPCODE_ADD, &docu_addset_grammar, docu_addset_usage, docu_ops },
	{ "set", OPCODE_SET, &docu_addset_grammar, docu_addset_usage, docu_ops },
	{ "del", OPCODE_DEL, &docu_delget_grammar, docu_delget_usage, docu_ops },
	{ "get", OPCODE_GET, &docu_delget_grammar, docu_delget_usage, docu_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

static const struct cmdparse_action group_syntax [] = {
	{ "add", OPCODE_ADD, &group_addset_grammar, group_addset_usage, group_ops },
	{ "set", OPCODE_SET, &group_addset_grammar, group_addset_usage, group_ops },
	{ "del", OPCODE_DEL, &group_delget_grammar, group_delget_usage, group_ops },
	{ "get", OPCODE_GET, &group_delget_grammar, group_delget_usage, group_ops },
		/* end marker */
	{ NULL, 0, NULL, NULL, NULL }
};

static const char *a2rule_usage = "...  Manages database storage of ARPA2 Policy Rules\n"
	"ruleset     ...  Manages rules for ARPA2 Policy Rules (abstractly)\n"
	"communicate ...  Manages rules for ARPA2 Communication Access\n"
	"document    ...  Manages rules for ARPA2 Document Access\n"
	"group       ...  Manages rules for ARPA2 Groups";

static const struct cmdparse_class a2rule_syntax [] = {
	{ "ruleset",     ruleset_usage, ruleset_syntax },
	{ "communicate", comm_usage,       comm_syntax },
	{ "document",    docu_usage,       docu_syntax },
	{ "group",       group_usage,     group_syntax },
		/* end marker */
	{ NULL, NULL, NULL }
};



int main (int argc, char *argv []) {
	//
	// Invoke the cmdparser for <class> <action> [<keyword> <value>]...
	bool ok = cmdparse_class_action (argc, argv, a2rule_usage, a2rule_syntax);
	//
	// Return 1 for failure or 0 for success
	return (ok ? 0 : 1);
}

