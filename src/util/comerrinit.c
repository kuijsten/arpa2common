/* Initialise the COM-ERR library.
 *
 * We like to use the more advanced com_err codes, and pass them in errno,
 * but there are a few libraries.  Some may need to be initialised _if_
 * they are loaded by dlopen() by another program than the binary loader.
 * This may happen in a shared library loaded into a modularised server,
 * if it does not call linker-setup initialisation code of such modules.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>

#include <arpa2/com_err.h>


/* TODO: CMake may flag distinct between COM-ERR variants. */

#ifdef __FreeBSD__

/*
 * arpa2comerr_init() for FreeBSD
 */
#warning "No port for arpa2comerr_init() yet for FreeBSD com_err"

#else

/*
 * arpa2comerr_init() for Linux and others
 */
void arpa2comerr_init (void) {
	extern struct et_list *_et_list;
	static bool a2ce_init = false;
	if (!a2ce_init) {
		a2ce_init = true;
		_et_list = NULL;
	}
}

#endif

