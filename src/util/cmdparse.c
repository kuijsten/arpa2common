/* Parse commands and <keyword> <value> pairs in various forms.
 * The same logic can also parse the command line, with overlapping
 * storage variables, to simplify movements back and forth.
 *
 * The format is always a keyword and a value.  Later we may also allow
 * space-separated values.  Parsers may work on commandline args or on
 * lines in an ASCII or UTF-8 file.  The output is an array of strings
 * and the grammar is checked to be one of a set of provided strings.
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include <errno.h>

#include <arpa2/except.h>

#include <arpa2/util/cmdparse.h>



unsigned cmdparse_word (char const **str, unsigned prevlen) {
	*str += prevlen;
	*str += strspn (*str, " \t");
	return (unsigned) strcspn (*str, " \t\n");
}


bool cmdparse_keyword_value (struct cmdparser *prs, char *keyword, char *value) {
	log_debug ("Parsing keyword \"%s\" with value \"%s\"", keyword, value);
	//
	// Iterate over keywords in the grammar
	const char * const *kwdv = prs->grammar->keywords;
	bool found = false;
	unsigned keywordlen = strlen (keyword);
	for (int kwdi = 0; kwdi < 32 ; kwdi++, kwdv++) {
		//
		// See if the keyword is a match
		if (*kwdv == NULL) {
			continue;
		}
		if (strncasecmp (keyword, *kwdv, keywordlen) != 0) {
			continue;
		}
		//
		// Avoid ambiguous keyword recognition (and prefix extensions...)
		if (found) {
			goto fail;
		}
		//
		// Avoid repeated keywords with different values
		if ((prs->values [kwdi] != NULL) &&
					(0 != strcmp (prs->values [kwdi], value))) {
			//MODEST// log_error ("Attempt to change keyword %s value", keyword);
			log_error ("Attempt to change keyword %s value from \"%s\" to \"%s\"", keyword, prs->values [kwdi], value);
			goto fail;
		}
		//
		// Avoid non-printable-ASCII and only allow spaces in values
		// when the keyword is flagged in grammar->listwords
		for (char *cursor = value; *cursor != '\0'; cursor++) {
			if (*cursor == ' ') {
				if ((prs->grammar->listwords & (1 << kwdi)) == 0) {
					log_error ("Not a spaced-list keyword: %s", keyword);
					goto fail;
				}
			} else if ((*cursor < 32) || (*cursor >= 127)) {
				log_error ("Value contains illegal character code 0x%02x", (int) *cursor);
				goto fail;
			}
		}
		//
		// Take note of the keyword
		prs->values [kwdi] = value;
		//
		// Take note of the keyword presence
		prs->combination |= (1 << kwdi);
		found = true;
	}
	//
	// Require that the keyword is found
	if (!found) {
		goto fail;
	}
	//
	// Return success
	return true;
	//
	// Report failure
fail:
	prs->error = true;
	return false;
}


bool cmdparse_combinations (struct cmdparser *prs) {
	//
	// Start with no masks required or acknowledged
	uint32_t goal  = 0;
	uint32_t score = 0;
	const uint32_t *pairs = prs->grammar->combinations;
	//
	// Iterate over mask/value pairs (if any given)
	if (pairs != NULL) {
		log_debug ("Combinations: have = %08x", prs->combination);
		while (*pairs != 0) {
			log_debug ("Combinations: mask = %08x, value = %08x", pairs [0], pairs [1]);
			//
			// Take note of the mask as a goal
			goal |= pairs [0];
			//
			// Try to score into the goal
			if ((prs->combination & pairs [0]) == pairs [1]) {
				score |= pairs [0];
			}
			log_debug ("Combinations: goal = %08x, score = %08x", goal, score);
			//
			// Next mask/value pair
			pairs += 2;
		}
	}
	//
	// Return whether the goal was reached
	if (score != goal) {
		prs->error = true;
		return false;
	} else {
		return true;
	}
}


bool cmdparse_kwargs (struct cmdparser *prs, int argc, char **argv) {
	if ((argc & 1) != 0) {
		log_error ("Keywords and values do not occur in pairs");
		return false;
	}
	for (int argi = 0; argi < argc; argi += 2) {
		cmdparse_keyword_value (prs, argv [argi+0], argv [argi+1]);
	}
	cmdparse_combinations (prs);
	return !prs->error;
}


bool cmdparse_string (struct cmdparser *prs, char *text) {
	//
	// Iterate over the text
	char *keyword = text;
	while (*keyword != '\n') {
		//
		// If this is the string end, something broke down
		if (*keyword == '\0') {
			goto fail;
		}
		//
		// Find the space or tab between keyword and value
		char *value = keyword + strcspn (keyword, " \t\n");
		if ((value == NULL) || (*value == '\n')) {
			goto fail;
		}
		*value++ = '\0';
		//
		// Quietly skip over superfluous spaces and tabs
		value += strspn (value, " \t");
		//
		// Find the next line and current line size
		char *nextline = strchr (value, '\n');
		if (nextline == NULL) {
			goto fail;
		}
		*nextline++ = '\0';
		//
		// We now have a keyword and a value
		if (!cmdparse_keyword_value (prs, keyword, value)) {
			goto fail;
		}
		//
		// Move on to the next line
		keyword = nextline;
	}
	//
	// After parsing the empty line, ensure that the string ends
	if (keyword [1] != '\0') {
		goto fail;
	}
	//
	// Terminate succesfully
	return true;
	//
	// Report parser failure
fail:
	log_error ("String parsing failed");
	prs->error = true;
	return false;
}


bool cmdparse_backstore (struct cmdparser *prs) {
	return cmdparse_string (prs, prs->backstore);
}


unsigned cmdparse_unparse (const struct cmdparser *prs, char *opt_buf, unsigned buflen) {
	unsigned totlen = 0;
	int restlen = buflen;
	//
	// Iterate over all variables
	for (int varnr = 0; varnr <= 31; varnr++) {
		//
		// Skip printing variables without a value
		if (prs->values [varnr] == NULL) {
			continue;
		}
		//
		// Be sure to have buffer space to print to
		char tmpbuf [10];
		if ((opt_buf == NULL) || (restlen <= 0)) {
			opt_buf =         tmpbuf ;
			restlen = sizeof (tmpbuf);
		}
		//
		// Strict program correctness: Do not write NULL keys
		assertxt (prs->grammar->keywords [varnr] != NULL,
				"Grammar has no keywords[%d] for value %s",
				varnr, prs->values [varnr]);
		//
		// Print a single field to the buffer
		int eltlen = snprintf (opt_buf, restlen,
					"%-9s %s\n",
					prs->grammar->keywords [varnr],
					prs->values [varnr]);
		if (eltlen < 0) {
			log_errno ("Failure to unparse request");
			return 0;
		}
		//
		// Update lengths and buffer position
		totlen  += eltlen;
		restlen -= eltlen;
		opt_buf += eltlen;
	}
	//
	// Append an empty line to allow incompleteness detection
	totlen += 1;
	if (restlen >= 2) {
		opt_buf [0] = '\n';
		opt_buf [1] = '\0';
	}
	//
	// Return the number of bytes that were / would have been printed
	return totlen;
}


void cmdparse_usage (const char *usage, int argi, char *argv []) {
	fprintf (stderr, "Usage: %s %s%s%s%s%s\n",
			argv [0],
			(argi >= 1) ? argv [1] : "",
			(argi >= 1) ? " "      : "",
			(argi >= 2) ? argv [2] : "",
			(argi >= 2) ? " "      : "",
			usage);
}


bool cmdparse_class_action (int argc, char *argv [],
			const char *usage, const struct cmdparse_class *clsarray) {
	int argi = 0;
	//
	// Require enough parameters
	if (argc < 3) {
		goto parser_error;
	}
	//
	// Look for argv [1]
	const struct cmdparse_class *class_found = NULL;
	while (clsarray->classstr != NULL) {
		if (strncmp (argv [1], clsarray->classstr, strlen (argv [1])) == 0) {
			if (class_found != NULL) {
				/* Ambiguous command */
				goto parser_error;
			}
			class_found = clsarray;
		}
		clsarray ++;
	}
	if (class_found == NULL) {
		goto parser_error;
	}
	argi++;
	//
	// Look for argv [2]
	usage = class_found->usage;
	const struct cmdparse_action *clsactarray = class_found->actions;
	const struct cmdparse_action *action_found = NULL;
	while (clsactarray->actionstr != NULL) {
		if (strncmp (argv [2], clsactarray->actionstr, strlen (argv [2])) == 0) {
			if (action_found != NULL) {
				/* Ambiguous command */
				goto parser_error;
			}
			action_found = clsactarray;
		}
		clsactarray++;
	}
	if (action_found == NULL) {
		goto parser_error;
	}
	short opcode = action_found->opcode;
	argi++;
	//
	// Construct the parser for the remaining keyword arguments
	usage = action_found->usage;
	struct cmdparser prs = NEW_CMDPARSER (action_found->grammar);
	if (!cmdparse_kwargs (&prs, argc-3, argv+3)) {
		goto parser_error;
	}
	//
	// We found a function to call for the actionfun
	usage = action_found->usage;
	bool retval = action_found->actionfun (opcode, &prs, usage, argc, argv);
	return retval;
	//
	// We hit a parser error
parser_error:
	cmdparse_usage (usage, argi, argv);
	return false;
}

