/* Iteration over Group Members, based on Policy Rules.
 *
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include "arpa2/except.h"
#include "arpa2/identity.h"
#include "arpa2/group.h"
#include "arpa2/rules.h"
#include "arpa2/rules_db.h"


rules_type rules_type_group = {
	0x5a, 0x1a, 0x25, 0x96, 0x17, 0x63, 0x36, 0xbf,
	0xa7, 0xb2, 0x81, 0x4a, 0xd9, 0x80, 0x83, 0xca
};


static bool fallback_selector_configured = false;
static a2sel_t fallback_selector;


void group_init (void) {
	if (!fallback_selector_configured) {
		memset (&fallback_selector, 0, sizeof (fallback_selector));
		if (!a2sel_parse (&fallback_selector, "@.", 2)) {
			log_errno ("Failed to parse \"@.\" as selector for group iteration");
		}
		fallback_selector_configured = true;
	}
}


void group_fini (void) {
	/* Nothing yet */ ;
}


/* Request structure for group iteration.
 */
struct groupiter_request {
	struct rules_request req;
	group_iterate_upcall *upcall;
	void *updata;
	const a2act_t *sender;
	const char **filters;
	group_marks required_marks;
	group_marks forbidden_marks;
};


/* Filter a member based on match rules.
 *
 * Match rules are formatted [nonplus][+mid+mid+-+mid+mid][@domain]
 * where
 *  - "nonplus" contains no "+" and may be a group, but usually empty
 *  - "mid" is a member identity, a single word
 *  - "+" is a separator of words (shown as part of a two-element list)
 *  - "+-+" toggles the mode pos->neg->pos->neg->...
 *  - "@domain" is treated as equivalently to the end-of-string
 *  - "[+mid...mid]" may be absent to accept any group member
 *
 * @param[in] filters is a NULL-terminated array of the above strings
 *
 * @param[in] member is the single member name to match, not NUL-terminated
 *
 * @param[in] memberlen is the number of characters in \a member
 *
 * @param[in] marks are the current markings for the \a member
 *
 * @returns true to pass the member, false to not pass it through
 */
static bool _filtermember (const char **filters, const char *member, unsigned memberlen, group_marks marks) {
	//
	// Remember if we were lucky enough to pass (not initially though)
	bool filter_out = false;
	//
	// Iterate over all filters, if need be
	while ((!filter_out) && (*filters != NULL)) {
		//
		// Find the first '+' or '@', if any
		const char *mid = *filters++;
		mid += strcspn (mid, "+@");
		//
		// Special case for just the group: all reading members
		if (*mid != '+') {
			/* Match the entire group */
			filter_out = ((marks & GROUP_RECV) != 0);
		}
		//
		// After every '+' compare with the member
		bool filter_pos = true;
		while (*mid == '+') {
			mid++;
			size_t midlen = strcspn (mid, "+@");
			if ((midlen == 1) && (*mid == '-')) {
				/* Toggle positive/negative for +-+ */
				filter_pos = ! filter_pos;
			}
			if ((midlen == memberlen) && (memcmp (mid, member, memberlen) == 0)) {
				/* Matching member, apply filter */
				filter_out = filter_pos;
			}
			mid += midlen;
		}
	}
	//
	// Return whether the member passed through the filters
	return filter_out;
}


/* Callback for Trigger.
 *
 * The trigger returns without action for missing required_marks or
 * presented forbidden_marks.
 */
static bool _itertrigger (struct rules_request *req, rules_trigger name, unsigned namelen) {
	//
	// Expand the request data type
	struct groupiter_request *greq = (struct groupiter_request *) req;
	//
	// Return now if marks are off
	if ((req->flags & greq->forbidden_marks) != 0) {
		/* Some flags are unwanted, continue scanning */
		return true;
	}
	if ((greq->required_marks & ~req->flags) != 0) {
		/* Some flags are missing, continue scanning */
		return true;
	}
	//
	// Find the '@' that separates the member name from the delivery address
	const char *atsign = (char *) memchr (name, '@', namelen);
	if ((atsign == NULL) || (atsign == name) || (atsign == name + namelen-1)) {
		/* No '@' sign or at the start or the first is at the end */
		return true;
	}
	//
	// Locate strings member[memberlen] and delivery[deliverylen]
	const char *member = name;
	unsigned memberlen = atsign - name;
	const char *delivery = atsign + 1;
	unsigned deliverylen = namelen - memberlen - 1;
	//
	// Apply the filters to the member under consideration
	if (!_filtermember (greq->filters, member, memberlen, req->flags)) {
		/* All filters disapproved of the member.  Continue working. */
		return true;
	}
	//
#if 0
	//
	// Map the member name to a group address
	a2act_t memid;
	memcpy (&memid, greq->sender, sizeof (memid));
	if (!textshift ((struct a2id_t *) &memid, A2ID_OFS_AT_DOMAIN, memid.ofs [A2ID_OFS_ALIAS] + memberlen, false)) {
		return false;
	}
	memcpy (memid.txt + memid.ots [A2ID_OFS_ALIAS], member, memberlen);
	for (unsigned o = A2ID_OFS_OPEN_END; o < A2ID_OFS_AT_DOMAIN; o++) {
		memid.ofs [o] = memid.ofs [A2ID_OFS_AT_DOMAIN];
	}
#else
	//
	// Lookup strings group[grouplen] and atdomain[atdomainlen]
	const char *group = greq->sender->txt + greq->sender->ofs [A2ID_OFS_USERID];
	unsigned grouplen = greq->sender->ofs [A2ID_OFS_PLUS_ALIASES] - greq->sender->ofs [A2ID_OFS_USERID];
	const char *atdomain = greq->sender->txt + greq->sender->ofs [A2ID_OFS_AT_DOMAIN];
	unsigned atdomainlen = greq->sender->ofs [A2ID_OFS_END] - greq->sender->ofs [A2ID_OFS_AT_DOMAIN];
	//
	// Map the member name to a group address
	char memstr [grouplen + 1 + memberlen + atdomainlen];
	sprintf (memstr, "%.*s+%.*s%.*s", grouplen, group, memberlen, member, atdomainlen, atdomain);
	a2act_t memid;
	if (!a2act_parse (&memid, memstr, sizeof (memstr), 1)) {
		/* Failure.  Possibly report.  Signal continuation. */
		return true;
	}
#endif
	//
	// Invoke the callback routine
	(void) greq->upcall (greq->updata, req->flags, greq->sender, &memid, delivery, deliverylen);
	//
	// Signal continuation
	return true;
}


/* The main function for group iteration.
 */
bool group_iterate (const a2act_t *sender, const char **filters,
		group_marks required_marks, group_marks forbidden_marks,
		const uint8_t *opt_svckey, unsigned svckeylen,
		const char *opt_rules, unsigned ruleslen,
		group_iterate_upcall upcall, void *updata) {
	//
	// Fill the request structure
	struct groupiter_request greq;
	memset (&greq, 0, sizeof (greq));
	greq.req.optcb_trigger = _itertrigger;
	greq.upcall = upcall;
	greq.updata = updata;
	greq.sender = sender;
	greq.filters = filters;
	greq.required_marks  = required_marks;
	greq.forbidden_marks = forbidden_marks;
	//
	// If opt_svckey is needed but NULL, setup the default
	rules_dbkey domkey;
	rules_dbkey svckey;
	if ((opt_svckey == NULL) && (opt_rules == NULL)) {
		if (!rules_dbkey_domain (domkey,
				NULL, 0,
				sender->txt + sender->ofs [A2ID_OFS_DOMAIN])) {
			return false;
		}
		if (!rules_dbkey_service (svckey,
				domkey, sizeof (domkey),
				rules_type_group)) {
			return false;
		}
		opt_svckey = svckey;
		svckeylen = sizeof (svckey);
	}
	//
	// Process the Ruleset
	bool ok = true;
	if (opt_rules == NULL) {
		unsigned namelen = sender->ofs [A2ID_OFS_PLUS_ALIASES] - sender->ofs [A2ID_OFS_USERID];
		char name [namelen + 1];
		memcpy (name, sender->txt + sender->ofs [A2ID_OFS_USERID], namelen);
		name [namelen] = '\0';
		assertxt (fallback_selector_configured,
				"The selector \"@.\" for group iteration was not prepared by group_init()");
		ok = ok && rules_dbiterate (&greq.req,
				opt_svckey, svckeylen,
				name,
				&fallback_selector);
	} else {
		ok = ok && rules_process (&greq.req,
				opt_rules, ruleslen,
				false);
	}
	//
	// Produce the return value
	return ok;
}


/* Internal.  Callback for matching a member that was sought.
 *
 * This is a group_iterate_upcall function.
typedef bool group_iterate_upcall (
		void *updata,
		group_marks marks,
		const a2act_t *sender,
		const a2act_t *recipient,
		const char *delivery,
		unsigned deliverylen);
 *
 * The sender is set to the potential_member, and the group is
 * being iterated.
 */
bool _hasmember_upcall (void *_void_member_marks,
		group_marks marks,
		const a2act_t *potential_member,
		const a2act_t *recipient,
		const char *_delivery, unsigned _deliverylen) {
	//
	// Copy the marks if the recipient matches the potential_member
	log_detail ("group_hashmember() comparison between %s and %s", potential_member->txt, recipient->txt);
	if (strcmp (potential_member->txt, recipient->txt) == 0) {
		(* (group_marks *) _void_member_marks) = marks;
	}
	//
	// Return continued searching
	return true;
}


/* Test if a member is subcribed to a group; this also tests the existence
 * of a group.
 */
bool group_hasmember (const a2act_t *potential_member,
		const uint8_t *opt_svckey, unsigned svckeylen,
		const char *opt_rules, unsigned ruleslen,
		group_marks *out_marks) {
	log_detail ("Testing for Group Member existence on %s", potential_member->txt);
	//
	// Form a NULL-terminated filters array for the member itself
	const char *me_only_filter [] = { potential_member->txt, NULL };
	//
	// Start with the lowest rights, overwritten when finding the member
	*out_marks = GROUP_VISITOR;
	//
	// Iterate over the group
	bool iterok = group_iterate (potential_member, me_only_filter,
		0, 0,
		opt_svckey, svckeylen, opt_rules, ruleslen,
		_hasmember_upcall, (void *) out_marks);
	//
	// Return the error condition, setting the marks to GROUP_VISITOR on error
	if (!iterok) {
		*out_marks = GROUP_VISITOR;
	}
	return iterok;
}

