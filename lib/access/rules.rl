/* ARPA2 Access Control
 *
 * This parses Access Control Lists, as stored in a key-value database,
 * scoped under ([dbkey],access_domain,access_type) and further
 * searched with the access_key.
 *
 * For example, the Communication Access uses:
 *   - access_type   == b4f0fc38-d4d7-3bb9-ad69-5bf75efc46dd (16 bytes)
 *   - access_name   == userid or +svcname
 *   - access_domain == local address domain
 * and lists variables defining:
 *   - 'a' for alias/svcarg to match
 *   - 'o' for alias/svcart override
 *   - 's' with minimum signature flags to meet
 *   - 'n' for new alias/svcarg
 *
 * SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#include <stdbool.h>
#include <stdint.h>

#include <lmdb.h>

#include <arpa2/com_err.h>
#include <com_err/arpa2identity.h>
#include <com_err/arpa2access.h>
#include <arpa2/except.h>

#include "arpa2/digest.h"
#include "arpa2/identity.h"

#include "arpa2/rules.h"
#include "arpa2/rules_db.h"
#include "arpa2/access.h"


%%{

	# Following is the machinery for ARPA2 Rules which
	# is used for Access Control, Groups, ...
	#
	machine arpa2rules ;

	# Whitespace is simply skipped on pleasant points.
	# Size does not matter, and whether it is optional
	# can be decided on the place where this is inserted.
	#
	sp = ' ' ;

	# Rules are terminated with a NUL byte.  Lists may continue
	# after this, but only when characters remain.  Lists may in
	# fact be empty, precisely because the NUL byte always exists
	# at the end of any entry (even an empty string entry).
	nul = 0x00 ;

	# Reset access info, done at the Ruleset start and after
	# every Rule ended with a NUL character.
	# The varray_str[] pointers are never set to NULL, but all
	# varray_strlen[] are set to 0.
	#
	reset = ""
		%{ req->flags = 0;
		   req->flags_rawstr = "";
		   for (int kv = 0; kv < 26; kv++) {
			req->varray_str    [kv] = "";
			req->varray_strlen [kv] = 0;
		  } }
		;
	#
	nul_reset = nul . reset
		>{ if (req->optcb_endrule != NULL) {
			assertxt (req->optcb_endrule (req),
				"Upcall for end of Rule"); } }
		;

	# Access Rights, specified as a series of uppercase letters.
	# These will be taken in as bits (A==0, B==1, ...) in an uint32_t.
	# Bits 26..31 are reserved for future use, write zero and ignore.
	#
	# A declaration of flags is also the normal point that triggers a
	# callback to the requesting party.  Note that flags declared in
	# an initiation line will cause a callback for followin production
	# lines.
	#
	flags = '%' . [A-Z]*
		>{ req->flags = 0;
		   req->flags_rawstr = p; }
		${ req->flags |= RULES_FLAG (fc); }
		%{ if (req->optcb_flags != NULL) {
			assertxt (req->optcb_flags (req),
				"Upcall for Rule %%%.*s",
				(int) (p-req->flags_rawstr), req->flags_rawstr); } }
		;

	# Set a variable to a liberal string value.  This is intended to
	# relay very simple forms of information, and the interpretation
	# is specific to an Access Type.
	#
	# There are 26 variables (named as lowercase letters) that will
	# be stored for later callbacks, usually when flags are found.
	#
	# Note that values lack quotes and escapes, so if you need even
	# more liberal values you should use an encoding and be specific
	# about it.  All URIs and base64 are welcome.
	#
	value = ( 0x21..0x7e ) * ;
	#
	keptvar = '=' . [a-z] . value
		>{ keptidx = RULES_VARINDEX (p [-1]);
		   req->varray_str    [keptidx] = p; }
		%{ req->varray_strlen [keptidx] = (p - req->varray_str [keptidx]); }
		;
	#
	trigger = '^' . value
		>{ callarg = p; }
		%{ if (req->optcb_trigger == NULL) {
			errno = A2XS_ERR_RULE_MISSING_CALLBACK;
			goto ragel_exit;
		   }
		   assertxt (req->optcb_trigger (req, callarg, (p - callarg)),
				"Upcall for ^%.*s",
				(int) (p - callarg), callarg);
		 }
		;

	# Disable an entry by prefixing it with a '#'.
	# This does not continue until the end of line!
	#
	disable = '#' . value ;

	# Selector, which may occur in an LDAP declaration.
	#
	selector = '~' . value
		>{ callarg = p; }
		%{ if (!permit_selector) {
			log_error ("Rule declares %.*s while context also defines a Selector",
					(int) (p - callarg), callarg);
			errno = A2XS_ERR_RULE_FORBIDDEN_SELECTOR;
			goto ragel_exit;
		 } else if (req->optcb_selector != NULL) {
			a2sel_t rmsel;
			if (a2sel_parse (&rmsel, callarg, p - callarg)) {
				assertxt (req->optcb_selector (req, &rmsel),
					"Upcall for ~%.*s",
					(int) (p-callarg), callarg);
			} else {
				/* errno == A2ID_ERR_SELECTOR_SYNTAX */
				log_error ("Invalid ~selector in Access Control: %.*s",
						(int) (p - callarg), callarg);
				goto ragel_exit; } } }
		;

	# Access Control Lists are found for a combination of...
	#   - [Database Key], Domain to access
	#   - Access Type
	#   - Access Name, Selector (which walks up)
	# ...and yield any number of...
	#   - Set Variables as defined for the Access Type
	#   - Declaration of Rights
	#
	decl = ( keptvar | trigger | flags | disable | selector )
		>{ declptr = p; }
		<err{
			const char *declend = declptr;
			while ((declptr < pe) && (*declend != ' ')) {
				declptr++;
			}
			log_error ("Wrong char at offset %d in decl \"%.*s\"",
				(int) (1 + p - declptr), (int) (declend - declptr), declptr);
			errno = A2XS_ERR_RULE_GRAMMAR;
			goto ragel_exit; }
		;
	#
	rule = decl . ( sp . decl )* . nul_reset
		%{ rulenr++; colnr = 1; }
		;
	#
	ruleset = reset . rule*
		%{ if (req->optcb_endruleset != NULL) {
			assertxt (req->optcb_endruleset (req),
				"Upcall for end of Rulset"); } }
		;
	#
	main := ruleset
		>{ rulenr = 1; colnr = 1; }
		${ colnr++; }
		$err{
			log_error ("Failed to parse rule %d, char %d",
				rulenr, colnr);
			errno = A2XS_ERR_RULE_GRAMMAR;
			goto ragel_exit; }
		;
}%%


%% write data;


bool rules_process (struct rules_request *req,
			const char *acl, unsigned acllen,
			bool permit_selector) {
	bool ok = true;
	//
	// Setup for parsing of "acl_rule"
	int cs, rulenr, colnr;
	const char *declptr;
	const char *p   = acl;
	const char *pe  = acl + acllen;
	const char *eof = pe;
	const char *callarg = NULL;
	int keptidx = 0;
	//
	// Invoke the parser -- it may reset "ok" and lookup "permit_selector"
	%%{
		machine arpa2rules ;
		write init ;
		write exec ;
	}%%
	//
	// Conclude if parsing succeeded
	ok = ok && (cs >= arpa2rules_first_final) && (p == pe);
/* ragel_done: (unused label) */
	return ok;
ragel_exit:
	//
	// Presumably, errno has been set
	return false;
}


bool rules_dbiterate (struct rules_request *req,
			const uint8_t *svckey, unsigned svckeylen,
			const rules_name name,
			const a2id_t *iter0) {
	//
	// Setup the request structure for this call
	req->optcb_selector = NULL;
	req->opt_name = name;
	req->opt_qiter.src = iter0;
	if (req->rules_process_override == NULL) {
		req->rules_process_override = rules_process;
	}
	//
	// Start digesting the prekey, and later fork its output state
	log_detail ("Rules DB iteration: For name %s, for selector %s, ...", name, iter0->txt);
	log_data ("Rule DB iteration: ...for Service Key", svckey, svckeylen, 0);
	bool ok = true;
	a2md_state midkey;
	ok = ok && a2md_open (&midkey);
	bool pre2midkey = ok;
	ok = ok && a2md_write_msg (&midkey, svckey, svckeylen);
	//
	// Extend the hash state with the Access Name
	ok = ok && a2md_write_msg (&midkey, name, strlen (name));
	//
	// Open the database in readonly mode
	struct rules_db ruledb;
	memset (&ruledb, 0, sizeof (ruledb));
	bool gotdb = rules_dbopen_rdonly (&ruledb);
	//
	// Iterator over the Identity
	bool found = false;
	MDB_val ruleset;
	if (ok && a2sel_quickiter_init (&req->opt_qiter)) do {
		ok = true;
		//
		// Clone the hash before expanding it
		a2md_state endkey;
		ok = ok && a2md_clone (&endkey, &midkey);
		bool mid2endkey = ok;
		//
		// Add the Selector
		ok = ok && a2md_printf (&endkey, "%.*s@%s",
				req->opt_qiter.uidlen,
				req->opt_qiter.uid,
				req->opt_qiter.dom);
		uint8_t dbkey [A2MD_OUTPUT_SIZE];
		//
		// Close down the expanded key
		if (mid2endkey) {
			bool got = a2md_close (&endkey, dbkey);
			ok = ok && got;
		}
		//
		// Skip ahead when ruleset did not load
		if (!ok) {
			continue;
		}
		log_data ("Rules DB lookup key", dbkey, sizeof (dbkey), false);
		//
		// Look in the Rules DB -- break on success
		ok = ok && rules_dbget (&ruledb, dbkey, &ruleset);
		found = ok && (ruleset.mv_size > 0);
		//
		// Try the next abstraction of remote identity
	} while ((!found) && a2sel_quickiter_next (&req->opt_qiter));
	//
	// If we found an Ruleset record, process it (do not permit ~selector)
	if (found) {
		ok = ok && req->rules_process_override (req,
				ruleset.mv_data, ruleset.mv_size, false);
	}
	//
	// Cleanup the record retrieved
	if (found) {
		; // Not really used -- rules_dbsuspend (&ruledb);
	}
	//
	// Cleanup the database
	if (gotdb) {
		rules_dbclose (&ruledb);
	}
	//
	// Cleanup the midkey if it was cloned
	if (pre2midkey) {
		a2md_close (&midkey, NULL);
	}
	//
	// If nothing was found, we signal A2XS_ERR_MISSING_RULESET
	if (ok && !found) {
		errno = A2XS_ERR_MISSING_RULESET;
		ok = false;
	}
	//
	// Return whether we had trouble, regardless of callbacks
	return ok;
}


static const access_rights nxrights = ~(
	ACCESS_RIGHT('A') |
	ACCESS_RIGHT('B') |
	ACCESS_RIGHT('C') |
	ACCESS_RIGHT('D') |
	ACCESS_RIGHT('E') |
	ACCESS_RIGHT('F') |
	ACCESS_RIGHT('G') |
	ACCESS_RIGHT('H') |
	ACCESS_RIGHT('I') |
	ACCESS_RIGHT('J') |
	ACCESS_RIGHT('K') |
	ACCESS_RIGHT('L') |
	ACCESS_RIGHT('M') |
	ACCESS_RIGHT('N') |
	ACCESS_RIGHT('O') |
	ACCESS_RIGHT('P') |
	ACCESS_RIGHT('Q') |
	ACCESS_RIGHT('R') |
	ACCESS_RIGHT('S') |
	ACCESS_RIGHT('T') |
	ACCESS_RIGHT('U') |
	ACCESS_RIGHT('V') |
	ACCESS_RIGHT('W') |
	ACCESS_RIGHT('X') |
	ACCESS_RIGHT('Y') |
	ACCESS_RIGHT('Z')
);

bool access_parse (char *userstr, access_rights *accumulator) {
	bool mode_add = true;
	access_rights accu = *accumulator;
	assertxt ((accu & nxrights) == 0, "access_parse() called with bad *accumulator value");
	while (*userstr != '\0') {
		switch (*userstr) {
		case '%':
			/* ignore */
			break;
		case '=':
			accu = 0;
			/* continue into the '+' case */
		case '+':
			mode_add = true;
			break;
		case '-':
			mode_add = false;
			break;
		default:
			if ((*userstr < 'A') || (*userstr > 'Z')) {
				errno = A2XS_ERR_ACCESS_RIGHTS_FORMAT;
				return false;
			}
			if (mode_add) {
				accu |=  ACCESS_RIGHT (*userstr);
			} else {
				accu &= ~ACCESS_RIGHT (*userstr);
			}
		}
		userstr++;
	}
	*accumulator = accu;
	return true;
}

void access_format (const char *opt_fmtstr, access_rights rights, char *outstr) {
	if (opt_fmtstr == NULL) {
		opt_fmtstr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	}
	char c;
	while (c = *opt_fmtstr++, c != '\0') {
		if ((c >= 'A') && (c <= 'Z')) {
			uint32_t mask = ACCESS_RIGHT (c);
			if ((rights & mask) != 0) {
				*outstr++ = c;
			}
		} else {
			*outstr++ = c;
		}
	}
	*outstr = '\0';
}


static bool initialised = false;

void rules_init (void) {
	if (!initialised) {
		initialize_A2XS_error_table ();
		initialised = true;
	}
}

void rules_fini (void) {
}
