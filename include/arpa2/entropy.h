/** @defgroup arpa2entropy Entropy suitable for Cryptography
 * @{
 *
 * Very simple wrappers that enable us to switch between backends.
 * Platforms all offer different access mechanisms for entropy,
 * and we cannot always rely on the use of a crypto library, or which,
 * so we prefer to stay low and use OS functionality.
 *
 * These wrappers are static inline functions with an a2entropy_ prefix.
 * This allows using a different instantiation in different files, by
 * defining a single symbol A2ENTROPY_STYLE before including this file.
 *
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ENTROPY_H
#define ARPA2_ENTROPY_H


#include <stdbool.h>
#include <stdint.h>


#ifdef __cplusplus
extern "C" {
#endif



/** @brief Initialise the crypto generator.
 *
 * Some platforms may require an explicit initialisation of the
 * entropy functions.  Providing a seed is not usually required
 * for crypto-aware random number generators, but a device may
 * need to be opened, or a scrambler set to avoid that tapping by
 * others could predict the eventually had entropy.
 *
 * You should call this function before your first call to
 * a2entropy_fast() or a2entropy_accounted() and you should mirror
 * it with a call to a2entropy_fini() for proper cleanup of any
 * remaining state (some of which may be sensitive).
 */
static inline void a2entropy_init (void);
//
#define A2ENTROPY_ACCOUNTED_INIT
#define A2ENTROPY_FAST_INIT


/** @brief Deactivate the crypto generator.
 *
 * This is the opposite function of a2entropy_init() and is run to
 * deactivate the a2entropy_ functionality as a whole, including
 * the cleanup of any remaining evidence of dispatched entropy.
 */
static inline void a2entropy_fini (void);
//
#define A2ENTROPY_ACCOUNTED_FINI
#define A2ENTROPY_FAST_FINI


/** @brief Generate relaxed random material for online use.
 *
 * Most applications of cryptography can work on light-weight random
 * material, influenced by actual entropy at unpredictable times, but
 * turned into a continuous flow of unbiased and surprising bits.
 *
 * By not accounting for the entropy behind every bit of output, this
 * allows for continuous generation of random material that a remote
 * would consider impossible to predict or influence, which is usually
 * sufficient.  Not blocking a network protocol until enough entropy
 * has arrived is vital to achieving reasonable throughput.
 *
 * Applications include session keys and salt/challenge generation,
 * and it may also be used for casually generated secrets.
 *
 * @returns true on success, false with errno set on failure.
 * @param   buffer points to the output buffer to be filled with random bytes.
 * @param   buffersize indicates the size of the output buffer up to 65536.
 */
static inline bool a2entropy_fast (uint8_t *buffer, unsigned buffersize);


/** @brief Generate offline random material with tightly controlled entropy.
 *
 * In rare cases, cryptography does require full accounting over
 * every bit of entropy used.  Since entropy must not be predictable
 * or subject to any external influence, this can be difficult.  If
 * hardware produces a good flow of entropy, then the removal of bias
 * tends to cause a flow that has no guaranteed minimum rate.
 *
 * Note that online offering of protocols that include this form of
 * entropy may lead to exhaustion of the available entropy, which in
 * effect would be a denial of service attack.  This is why online
 * services should always use the relaxed call.
 *
 * Applications include off-line generation of long-term keys, such
 * as for root certificates or KDCs.  It may also be used for the
 * most valuable server certificates, if so inclined, but the use of
 * this function leads to unpredictable processing delays and should
 * only be used for offline operations.
 *
 * @returns true on success, false with errno set on failure.
 * @param   buffer points to the output buffer to be filled with random bytes.
 * @param   buffersize indicates the size of the output buffer up to 256.
 */
static inline bool a2entropy_accounted (uint8_t *buffer, unsigned buffersize);




/** @def A2ENTROPY_FAST_IMPL
 * @brief Selection of the implementation for a2entropy_fast()
 *
 * @var A2ENTROPY_FAST_IMPL_GETRANDOM
 *      uses the getrandom() call defined on Linux
 */

#ifndef A2ENTROPY_FAST_IMPL
#error "Your build should configure an implementation for a2entropy_fast()"
#endif

#define A2ENTROPY_FAST_IMPL_GETRANDOM 1



/** @def A2ENTROPY_ACCOUNTED_IMPL
 * @brief Selection of the implementation for a2entropy_accounted()
 *
 * @var A2ENTROPY_ACCOUNTED_IMPL_GETENTROPY
 *      uses the getentropy() call defined as a Unix standard
 */

#ifndef A2ENTROPY_ACCOUNTED_IMPL
#error "Your build should configure an implementation for a2entropy_accounted()"
#endif

#define A2ENTROPY_ACCOUNTED_IMPL_FAIL 0
#define A2ENTROPY_ACCOUNTED_IMPL_GETENTROPY 1



#if A2ENTROPY_FAST_IMPL == A2ENTROPY_FAST_IMPL_GETRANDOM

/***** a2entropy_fast() with the getrandom() call from Linux *****/

#include <sys/random.h>
#include <errno.h>
static inline bool a2entropy_fast (uint8_t *buffer, unsigned buffersize) {
	if (buffersize > 65536) {
		errno = EINVAL;
		return false;
	}
	ssize_t gotten = getrandom (buffer, buffersize, 0);
	if (gotten == buffersize) {
		return true;
	} else if (gotten != -1) {
		errno = ENOSYS;
	}
	return false;
}

//TODO// #elif A2ENTROPY_FAST_IMPL == A2ENTROPY_FAST_IMPL_xxx


#else

/***** a2entropy_fast() has no implementation *****/

#error "No valid implementation configured for a2entropy_fast()"

#endif



#if A2ENTROPY_ACCOUNTED_IMPL == A2ENTROPY_ACCOUNTED_IMPL_GETENTROPY

/***** a2entropy_accounted() via UNIX standard call getentropy() *****/

#include <unistd.h>
#include <errno.h>

static inline bool a2entropy_accounted (uint8_t *buffer, unsigned buffersize) {
	if (buffersize > 256) {
		errno = EINVAL;
		return false;
	}
	return (0 == getentropy (buffer, buffersize));
}


//TODO// #elif A2ENTROPY_ACCOUNTED_IMPL == A2ENTROPY_ACCOUNTED_IMPL_xxx


#elif A2ENTROPY_ACCOUNTED_IMPL == A2ENTROPY_ACCOUNTED_IMPL_FAIL

#include <errno.h>
static inline bool a2entropy_accounted (uint8_t *buffer, unsigned buffersize) {
	errno = ENOSYS;
	perror ("a2entropy_accounted() is not implemented");
	exit (1);
}

#else

/**** a2entropy_accounted() has no implementation *****/

#error "No valid implementation configured for a2entropy_accounted()"

#endif



/***** Generic code *****/


static inline void a2entropy_init (void) {
	A2ENTROPY_ACCOUNTED_INIT
	A2ENTROPY_FAST_INIT
}

static inline void a2entropy_fini (void) {
	A2ENTROPY_FAST_FINI
	A2ENTROPY_ACCOUNTED_FINI
}


#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ENTROPY_H */

/** @} */
