/** @defgroup access_h Access Control for ARPA2 Identities
 * @{
 * Our blog discusses much background in the
 * [Access Control series](http://internetwide.org/tag/access.html)
 * and in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * We define an efficient mechanism to evaluate access
 * rights, based on the identity of the requester and
 * a description of what they are trying to use.  The
 * latter consists of an Access Domain, a general notion
 * of well-known service that we call Access Type, and
 * to specify this in more detail an Access Name which is
 * a string with additional detail.
 *
 * This is a minimal abstraction over a framework for
 * Policy Rules that also serves Groups, and possibly
 * more.  Further specialisation for kinds of Access
 * Control can subsequently be made for Commmunication,
 * File Access, and so on; these would be specific forms
 * of Access Type.
 *
 * Access Types specify semantics: they define how
 * Rules Flags are used as Access Rights; and how
 * Rules Variables define Attributes for Access Control;
 * and how Access Names are constructed.  The semantics
 * is implement these things in functions, so that a
 * semantic upgrade is possible with new software.
 */

/* SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ACCESS_H
#define ARPA2_ACCESS_H


#include <stdbool.h>
#include <stdint.h>

#include "arpa2/identity.h"
#include "arpa2/rules.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup access_h_types Access Types
 * @{
 * Access defines a number of types.  Several of them
 * are taken from a lower layer, namely the Rules.
 * This lower layer is more abstract and we redefine or
 * wrap several of these lower types to form higher-level
 * concepts.
 */



/** @brief Access Domains are UTF-8 representations
 * of a Fully Qualified Domain Name.
 *
 * These names have no initial or trailing dots, they
 * have at least one inner dot and they have been mapped
 * from the DNS wire format Punycode for international
 * names to UTF-8.  The wire form was a valid DNS name.
 *
 * Note the value of this mapping: Code should have only
 * one form to deal with, and Punycode is both specific
 * to one protocol and confusing.  It also cannot be
 * processed like any other internationalised code.  So
 * we choose an internal form that maps to a standard form.
 *
 * See A2ID_BUFSIZE for a discussion of the size that a domain
 * in this form can take.  This is normally supported as part
 * of the a2id_t and a2sel_t definitions in ARPA2 software.
 *
 * The NULL value is invalid as an Access Domain.  This
 * means that software can use it to indicate absense of a
 * domain in places where it accepts optional parameters.
 *
 * This definition is the same as for rules_domain.  It can
 * be stored in LDAP as an accessDomain attribute.
 */
typedef rules_domain access_domain;


/** @brief Access Types are 128-bit "well-known service
 * identities".
 *
 * We write an Access Type in UUID form, but code that
 * prepares for taking it up use the canonical form of
 * 16 bytes of which the UUID shows the per-byte hex form
 * with a couple of dashes.  These 16 bytes are used in
 * message digests.
 * 
 * The Access Type serves as a "well-known identifier"
 * for a class of uses.  For instance, we might have one
 * for the idea of "file storage" that could span many
 * application areas but be the same across vendors and
 * protocols and software suites.  Or we might have one
 * for "git access", regardless of the method being SSH,
 * HTTPS, local file system or other.
 *
 * Access Types need further refinment to properly
 * localise them.  First of all, the rules_domain has to be
 * distinguished, so policy rights assigned in one domain
 * cannot pass over to another domain.  Furthermore, every
 * Access Types defines a UTF-8 string format for an
 * access_name that can distinguish instances within the
 * well-known class below the domain.  In the Git example,
 * the Access Name could be a name for a repository.
 *
 * The all-zero value is not a valid access_type.  This
 * means that zeroing an rules_type will not cause presumptious
 * use of a valid access_type.
 *
 * This definition is the same as for rules_type.
 */
typedef rules_type access_type;


/** @brief Access Names represent instances of an Access Type.
 *
 * Where the Access Type is "well-known", it is instantiated by
 * an access_domain and this Access Name.  The general Access Name
 * format is a UTF-8 string with a NUL terminator, but its format
 * is defined by the Access Type.
 *
 * The purpose of the Access Name is to allow generic software
 * to fill it, perhaps with user-supplied templates or regular
 * expressions taking content from an application's context.
 * This allows the generation of an Access Name even in applications
 * that are not aware of the semantics of an Access Type.  As an
 * example, a generic web server may take elements from the path
 * and/or query paramters and use those in a string templated that
 * is printed into an environment variable to be supplied to some
 * software for Access Control.
 *
 * The value NULL is not a proper access_name.  Depending on the
 * definition for the given access_type, an empty string may be
 * a valid form.  Note that this means that NULL can be used to
 * indicate absense of an Rules Name in places where software
 * can optionally handle Rules Names.
 *
 * This definition is the same as for rules_name.
 */
typedef rules_name access_name;


/** @brief Access Rights in a bit mask.
 *
 * Access may set Rights named `A` through `Z` in short `%FLAGS`
 * words.  Encode `A` in bit 0 through `Z` in bit 25 of a
 * 32-bit unsigned integer.
 *
 * This definition is the same as rules_flags.
 */
typedef rules_flags access_rights;


/** @brief Map an uppercase letter to an Access Right.
 *
 * Use bit 0 for 'A', bit 1 for 'B' and so on.  Only
 * capital letters are mapped, and the caller should be
 * certain not to map anything else to a Rules Flag.
 *
 * This definition is the same as RULES_FLAG().
 */
#define ACCESS_RIGHT(c) ( 1 << (c - 'A') )


/* Rights to resources, from the highest grade to lower,
 * though it is up to the resource settings to actually
 * allow lower-grade rights along with higher-grade
 * rights like set with the _DOWN definitions below -- or
 * they may choose to withhold those implied privileges.
 * Applications have the opposite option; they may reason
 * that higher rights grant lower privileges, and use the
 * _UP macros to derive the bit mask to work with.  There
 * is no harm in combining these approaches.
 *
 * ACCESS_ADMIN is the highest privilege, reserved for humans
 *	with overall access to everything.
 *
 * ACCESS_SERVICE is the highest automation privilege, reserved
 *	for bots and such performing administrative changes.
 *
 * ACCESS_CONFIGURE is the right to modify the configuration of
 *	a service.
 *
 * ACCESS_OPERATE is the right to start or stop a service, not
 *	knowing anything about its contents or resources.
 *
 * ACCESS_DELETE is the right to delete a resource.  This is
 *	the highest resource right because it can undo all
 *	the other levels.
 *
 * ACCESS_CREATE is the right to create a resource.  Other than
 *	perhaps defining a name or retrieving an identity is
 *	not permitted with this, and certainly no changes.
 *
 * ACCESS_EXECUTE is the right to run a service or make a
 *	resource do something, such as accepting connections.
 *
 * ACCESS_WRITE is the right to write to an existing resource.
 *
 * ACCESS_READ is the right to read from an existing resource.
 *
 * ACCESS_PROVE is the right to prove properties about the
 *	resource.  An example could be a password check
 *	without actually seeing a key stored for that use.
 *
 * ACCESS_KNOW is the right to know about the existence of a
 *	resource.  It is not the right to know anything about
 *	its contents, nor to create or delete resources.
 *
 * ACCESS_OWNER is the right to own a resource.  This may sound
 *	like a lot, but it really does not allow to do anything
 *	with it.  A directory or an account might have this
 *	right, for instance.  It is mostly useful to pin the
 *	owner on the resource.
 *
 * ACCESS_VISITOR is the lowest of all rights.  It allows no actual
 *	operations, not even to know whether a resource exists.
 *	It is the ultimate send-away ("kluitje in het riet") right.
 *
 * ACCESS_FORBIDDEN is not even a proper level.  It simply has no
 *	flags at all, and is a downright negative, "damn all"
 *	response.  Try to be careful about this one; normally
 *	you might be able to include a cautious ACCESS_VISITOR
 *	level.
 */
#define ACCESS_ADMIN   ACCESS_RIGHT('A')
#define ACCESS_SERVICE ACCESS_RIGHT('S')
#define ACCESS_CONFIG  ACCESS_RIGHT('F')
#define ACCESS_OPERATE ACCESS_RIGHT('T')
#define ACCESS_DELETE  ACCESS_RIGHT('D')
#define ACCESS_CREATE  ACCESS_RIGHT('C')
#define ACCESS_EXECUTE ACCESS_RIGHT('X')
#define ACCESS_WRITE   ACCESS_RIGHT('W')
#define ACCESS_READ    ACCESS_RIGHT('R')
#define ACCESS_PROVE   ACCESS_RIGHT('P')
#define ACCESS_KNOW    ACCESS_RIGHT('K')
#define ACCESS_OWNER   ACCESS_RIGHT('O')
#define ACCESS_VISITOR ACCESS_RIGHT('V')
//
#define ACCESS_FORBIDDEN 0


/* The ACL rights with their higher rights added, to allow for
 * easier tests of inclusiveness.
 */
#define ACCESS_ADMIN_UP   ( ACCESS_ADMIN   | 0               )
#define ACCESS_SERVICE_UP ( ACCESS_SERVICE | ACCESS_ADMIN_UP   )
#define ACCESS_CONFIG_UP  ( ACCESS_CONFIG  | ACCESS_SERVICE_UP )
#define ACCESS_OPERATE_UP ( ACCESS_OPERATE | ACCESS_CONFIG_UP  )
#define ACCESS_DELETE_UP  ( ACCESS_DELETE  | ACCESS_OPERATE_UP )
#define ACCESS_CREATE_UP  ( ACCESS_CREATE  | ACCESS_DELETE_UP  )
#define ACCESS_EXECUTE_UP ( ACCESS_EXECUTE | ACCESS_CREATE_UP  )
#define ACCESS_WRITE_UP   ( ACCESS_WRITE   | ACCESS_EXECUTE_UP )
#define ACCESS_READ_UP    ( ACCESS_READ    | ACCESS_WRITE_UP   )
#define ACCESS_PROVE_UP   ( ACCESS_PROVE   | ACCESS_READ_UP    )
#define ACCESS_KNOW_UP    ( ACCESS_KNOW    | ACCESS_PROVE_UP   )
#define ACCESS_OWNER_UP   ( ACCESS_OWNER   | ACCESS_KNOW_UP    )
#define ACCESS_VISITOR_UP ( ACCESS_VISITOR | ACCESS_OWNER_UP   )


/* The ACL rights with their lower rights added, to allow for
 * easier tests of inclusiveness.
 */
#define ACCESS_ADMIN_DOWN   ( ACCESS_ADMIN   | ACCESS_SERVICE_DOWN )
#define ACCESS_SERVICE_DOWN ( ACCESS_SERVICE | ACCESS_CONFIG_DOWN  )
#define ACCESS_CONFIG_DOWN  ( ACCESS_CONFIG  | ACCESS_OPERATE_DOWN )
#define ACCESS_OPERATE_DOWN ( ACCESS_OPERATE | ACCESS_DELETE_DOWN  )
#define ACCESS_DELETE_DOWN  ( ACCESS_DELETE  | ACCESS_CREATE_DOWN  )
#define ACCESS_CREATE_DOWN  ( ACCESS_CREATE  | ACCESS_EXECUTE_DOWN )
#define ACCESS_EXECUTE_DOWN ( ACCESS_EXECUTE | ACCESS_WRITE_DOWN   )
#define ACCESS_WRITE_DOWN   ( ACCESS_WRITE   | ACCESS_READ_DOWN    )
#define ACCESS_READ_DOWN    ( ACCESS_READ    | ACCESS_PROVE_DOWN   )
#define ACCESS_PROVE_DOWN   ( ACCESS_PROVE   | ACCESS_KNOW_DOWN    )
#define ACCESS_KNOW_DOWN    ( ACCESS_KNOW    | ACCESS_OWNER_DOWN   )
#define ACCESS_OWNER_DOWN   ( ACCESS_OWNER   | ACCESS_VISITOR_DOWN )
#define ACCESS_VISITOR_DOWN ( ACCESS_VISITOR | 0                 )


/** @} */




/** @defgroup access_h_functions Access Functions
 * @{
 * Access defines some general functions.
 */


/** @brief Parse a string of Access Rights flags.
 *
 * Flags are uppercase letters, and assigned or set and cleared
 * in the accumulator, depending on characters in the string.
 * The parser can handle user input safely, returning an error
 * and making no changes in case of syntax problems.
 *
 * @param[in] flagstr is the string of flags.  It consists of
 *	uppercase flag characters, prefixed with instructions
 *	to set flags after '+', to clear flags after '-' or to
 *	start from zero after '='.  The character '%' is not
 *	required but will be silently ignored.  Instructions
 *	are processed in their order of appearance and the
 *	default mode is '+', so you "RW-CD" is a legal string.
 * @param[inout] accumulator collects the flags, and must be
 *	initialised before starting, to be safe in situations
 *	where the flagstr contains no '=' instruction.
 *
 * @returns This function returns false with a com_err in
 * errno in case of problems.  In case of success, it returns 
 * true.
 */
bool access_parse (char *userstr, access_rights *accumulator);



/** @brief Format Access Rights flags to a string.
 *
 * This prints to outstr the characters of opt_fmtstr if provided,
 * or otherwise up to 26 characters for a trailing NUL character.
 * The buffer to hold outstr must be large enough.
 *
 * This function does not fail.
 *
 * @param[in] opt_fmtstr holds uppercase letters and any other
 *	content that should appear in the outstr.  Uppercase
 *	characters are skipped when the corresponding flag in
 *	rights is clear.  Other characters are always added.
 *	When this parameter is NULL, all the flags are tried
 *	in alphabetic order, but no '%' or other character is
 *	prefixed.
 * @param[in] rights holds the access rights to filter which
 *	uppercase characters from the opt_fmtstr are added to
 *	the outstr.
 * @param[out] outstr is a buffer for the formatted output, and
 *	it must be large enough to hold the resulting string and
 *	a trailing NUL.  This means that it should be at least
 *	one character longer than the string length of opt_fmtstr
 *	or, if that is absent, a buffer size of 27.
 */
void access_format (const char *opt_fmtstr, access_rights rights, char *outstr);


/** @brief Initialise the ARPA2 Access system.
 *
 * This has a mirror image for cleaup in access_fini().
 */
static inline void access_init (void) {
	rules_init ();
}


/** @brief Finalise the ARPA2 Access system.
 *
 * This may do cleanup operations.
 */
static inline void access_fini (void) {
	rules_fini ();
}


/** @brief Lookup the realm for a fully qualified host name.
 *
 * The realm is
 * often a trailer of the hostname, though local conventions may
 * differ; a domain may be used as a hostname, and shared servers
 * may not even show overlap with the domain.  The mechanisms for
 * determining this may involve DNS records (with DNSSEC security)
 * or a configuration database, or merely a textual mapping.
 *
 * This function is used by protocols that communicate host names
 * instead of domain names.  A typical example is HTTP.  On such
 * systems, aliases must not interfere with the specified host
 * names; the best chance for that is to not lookup the client-sent
 * host name, but the configured canonical host name (web servers
 * tend to have a canonical ServerName and additionally allow
 * ServerAlias names that are also recognised as Host: header).
 *
 * This function returns NULL if no realm could be found for the
 * supplied host name.
 */
const char *access_host2domain (const char *fqdn_hostname);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ACCESS_H */

/** @} */
