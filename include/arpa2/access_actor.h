/** @ingroup access_h
 * @{
 * @defgroup access_actor_h Access Control for Acting-on-behalf-of
 * @{
 * Our blog discusses much background in the
 * [Access Control series](http://internetwide.org/tag/access.html)
 * and in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * Actors are Identities that take on the role of another.  After
 * login as john@example.com, it may be useful to act on behalf of
 * sales@example.com.  Whether this is permitted is posed as a
 * question to the Actor API.
 *
 * This relates to Network Address Translation, applied not to TCP/IP
 * addresses but to ARPA2 Identities.  The idea of NAT is harmful to
 * limited-space rewrites but mostly harmless when it is bidirectional
 * and static; the same is true for ARPA2 Identities, which may be
 * mapped one-on-one between internal and external Identities, where
 * the internal one is then reserved for just that purpose.  The easy
 * allocation of aliases allows this without adding much extra.
 *
 * ARPA2 Identities with NAT can be used to act as a group member, or
 * to allow external users to take on internal identities.  They can
 * can also be used as a better alternative to forwarding.  All this
 * makes it possible to always use domain-internal addresses, so that
 * systems like DKIM or SPF which make assumptions about the origin of
 * content based on a domain name can be used without their usual flaws.
 */

/* SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ACCESS_ACTOR_H
#define ARPA2_ACCESS_ACTOR_H


#include <stdbool.h>
#include <stdint.h>

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"


#ifdef __cplusplus
extern "C" {
#endif


TODO:FROMHERE


/** @defgroup access_h_types Communication Access Rights
 * @{
 * Communication Access defines a number of Access Rights.
 * They are based on the general Access Rights, though
 * with a somewhat technically-driven interpretation.
 *
 * The ARPA2 Identity to which Communication Access is
 * applied is the Remote Identity; the attempted contact
 * is to a Local Identity, from which the Access Domain
 * is derived.
 *
 * The Access Key is the userid part of the Local Identity
 * if it is a user, or the +service if it is a service.
 * Aliases and signatures are not included in the Access Key,
 * because they are too variable for database lookups.
 *
 * Attributes (Rule Variables) are used to constrain and
 * override aliases or service arguments, and to impose
 * minimum requirements on the signatures flags.
 */


/** @brief Access Rights for Communication
 * 
 * The access rights for communication each specify a list
 * that the remote party should land it.  This may be any
 * combination of white, black and grey list and there is
 * even a definition that suggests landing the remote in a
 * honeypot, which might be useful in attack situations.
 *
 * Normally, a request would be interpreted as being on any
 * one of these lists, and indeed, the highest available can
 * be selected in these cases.
 *
 * @def ACCESS_COMM_WHITELIST
 * @brief Marks accepted communication
 *
 *      Just like
 *	the other communication rights, this is agnostic to
 *	the communication protocol being used, so whitelist
 *	entries for email and chat are all the same.  This
 *	is thought to make sense and minimise the burden of
 *	contact management on the communication ACL.
 *      (This maps to ACCESS_WRITE.)
 *
 * @def ACCESS_COMM_GRAYLIST
 * @brief An alias for ACCES_COMM_GREYLIST
 *
 * @def ACCESS_COMM_GREYLIST
 * @brief Marks communication that should be subjected to an extra check
 *
 *      Think of the
 *	grey-listing by temporarily refusing to accept email,
 *	in a manner that is protocol-specific and only as an
 *	suggested approach.  It is up to the actual protocol
 *	to decide how to handle gray-listing.
 *	(This maps to ACCESS_CREATE -- as it can create a new
 *	 access privilege.)
 *
 * @def ACCESS_COMM_BLACKLIST
 * @brief This marks refused communication
 *
 *      This tends
 *	to be the cautious default.  Users who welcome general
 *	communication may set the selector "@." to pass anyone,
 *	and still be able to add rejection for more specific
 *	askers.
 *	(This maps to ACCESS_VISITOR, a rather useless right.)
 *
 * @def ACCESS_COMM_HONEYPOT
 * @brief This marks communication for side-tracking to a honeypot
 *        or other active form of suppression or investigation.
 *
 *      This may be used for remotes who are known offensive and are
 *	ideally side-tracked into a trap, or a honeypot or other
 *	structure in support of observation.  This may not be
 *	possible everywhere, in which case it may be treated
 *	just like ACCESS_COMM_BLACKLIST.
 *	(This maps to ACCESS_KNOW -- the hosted identity would be
 *	 shown to the remote party.)
 */
#define ACCESS_COMM_WHITELIST  ACCESS_WRITE
#define ACCESS_COMM_GRAYLIST   ACCESS_CREATE
#define ACCESS_COMM_GREYLIST   ACCESS_CREATE
#define ACCESS_COMM_BLACKLIST  ACCESS_VISITOR
#define ACCESS_COMM_HONEYPOT   ACCESS_KNOW
//
#define ACCESS_COMM_WHITELIST_UP  ACCESS_WRITE_UP
#define ACCESS_COMM_GRAYLIST_UP   ACCESS_CREATE_UP
#define ACCESS_COMM_GREYLIST_UP   ACCESS_CREATE_UP
#define ACCESS_COMM_BLACKLIST_UP  ACCESS_VISITOR_UP
#define ACCESS_COMM_HONEYPOT_UP   ACCESS_KNOW_UP
//
#define ACCESS_COMM_WHITELIST_DOWN  ACCESS_WRITE_DOWN
#define ACCESS_COMM_GRAYLIST_DOWN   ACCESS_CREATE_DOWN
#define ACCESS_COMM_GREYLIST_DOWN   ACCESS_CREATE_DOWN
#define ACCESS_COMM_BLACKLIST_DOWN  ACCESS_VISITOR_DOWN
#define ACCESS_COMM_HONEYPOT_DOWN   ACCESS_KNOW_DOWN


/** @brief Attributes for Communication
 *
 * Communication does not only define rights, but keeps
 * variables as well, to provide alongside callbacks
 * sharing these rights with the application.  These
 * variables are declared as part of the ACL text, in
 * which they occur as single-letter assignments.
 *
 * ACCESS_COMM_ALIAS or ACCESS_COMM_ARGS can be used to
 *	match the existence and value of user alias
 *	and/or service arguments.  The value is matched
 *	for a given userid or service name, so which
 *	applies is known when this is declared.  The
 *	text does not start with a '+' but it may
 *	cover multiple levels by haing internal '+'
 *	separators.  The local identity that is being
 *	addressed may add more alias words after this
 *	one, by adding even more '+' separators in the
 *	aliases field.
 *
 * ACCESS_COMM_NEWALIAS or ACCESS_COMM_NEWARGS can be used
 *	to overwrite the alias and/or service args.
 *	This can be used to redirect traffic to a
 *	sub-user or sub-service that is better suited
 *	to the request.  The original aliases or args
 *	vanish.
 *
 * ACCESS_COMM_SIGFLAGS can be used to require minimal
 *	support of certain signature flags, as well as
 *	to demand that a signature is actually present
 *	in the addressed local identity.  The check is
 *	on the presence of flags alone; the signature
 *	validation should independently be made, using
 *	the routine that silently skips over missing
 *	signatures; the combination with this check is
 *	useful to nonetheless enforce a signature, and
 *	require it to be good (and even personalised).
 *	The format of this field is decimal, 0x-prefixed
 *	hexadecimal or 0-prefixed octal and its value
 *	is a bitwise-OR of required A2ID_SIGFLAG_ values.
 */
#define ACCESS_COMM_ALIAS    RULES_VARINDEX ('a')
#define ACCESS_COMM_ARGS     RULES_VARINDEX ('a')
#define ACCESS_COMM_NEWALIAS RULES_VARINDEX ('o')
#define ACCESS_COMM_NEWARGS  RULES_VARINDEX ('o')
#define ACCESS_COMM_SIGFLAGS RULES_VARINDEX ('s')


/** @brief Access Types, the UUID codes that represent a well-known use case.
 */
extern access_type a2xs_type_comm;


/** @} */


/** @defgroup access_h_ops Communication Access Operations
 * @{
 * Communication Access can define Rules in two ways, namely
 * as a Localised Ruleset in connection to a Resource or in an
 * LDAP accessRule, or as Ruleset driven from a database.  The
 * two functions to implement these functions are similar to
 * the generic rules for Rules Processing, but they are more
 * specific:
 *
 *  - The Access Type is fixated to the one for Communication
 *  - The Access Key is derived from the Local Identity
 *  - The Local Identity can be changed by the Rules Variables
 *  - The Access Rigths are shown as a level, not as flags
 */


/** @brief Communication Access Request.
 */
struct access_comm_request {
	struct rules_request rlreq;
	a2id_t *local;
	a2id_t *remote;
};


/** @brief Communication Access Right is a level.
 */
typedef enum {
	access_comm_whitelist = ACCESS_COMM_WHITELIST,
	access_comm_greylist  = ACCESS_COMM_GREYLIST,
	access_comm_blacklist = ACCESS_COMM_BLACKLIST,
	access_comm_honeypot  = ACCESS_COMM_HONEYPOT,
} access_comm_list;



/* Check if a login_id can enter in the role of acl_id and
 * act like that for ACL rules.  This generally counts as
 * stepping down in the identity hierarchy.
 *
 * The return value is true when this change of identity is
 * permitted, false otherwise.  There is no strict reason
 * why there could not be a few of these calls in sequence,
 * but the operation is note generally reversible.  Once an
 * actor (login_id) takes on a role (acl_id) it cannot go
 * back up.
 */
bool a2xs_actor (void *dbh, const char *acl_id, const char *login_id);

//TODO// USE PARSED IDENTITIES


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ACCESS_COMM_H */

/** @} */
/** @} */
