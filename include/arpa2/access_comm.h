/** @ingroup access_h
 * @{
 * @defgroup access_comm_h Access Control for Communication
 * @{
 * Our blog discusses much background in the
 * [Access Control series](http://internetwide.org/tag/access.html)
 * and in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * We define several novel features to constrain communication
 * patterns with black/grey/white lists and even a honeypot sidetrack.
 * ARPA2 Selectors can be used to require certain remotes, such as
 * those under a domain name, or those with a base identity thereunder.
 *
 * We also define a form of signature in an ARPA2 Identity, which may
 * incorporate aspects of the communication pattern, to allow the
 * restricted use of a local identity.  Aspects that may be included
 * in the signature are the remote domain, remote userid, an expiration
 * day, topics or subject strings -- when included, they must match on
 * new submissions in order to be welcome.  This allows the creation
 * of addresses that cannot be handed over at will.
 *
 * The signature framework works in two phases; first, a signature
 * is always validated and blocked when it is wrong, but both absent
 * and correct signatures pass; second, access control can require a
 * minimum set of signature flags, thereby also requiring a signature,
 * when an Access Rules for Communication thinks it is a good idea,
 * such as for the catch-all "@." or a particularly suspect remote.
 */

/* SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_ACCESS_COMM_H
#define ARPA2_ACCESS_COMM_H


#include <stdbool.h>
#include <stdint.h>

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup access_comm_h_types Communication Access Rights
 * @{
 * Communication Access defines a number of Access Rights.
 * They are based on the general Access Rights, though
 * with a somewhat technically-driven interpretation.
 *
 * The ARPA2 Identity to which Communication Access is
 * applied is the Remote Identity; the attempted contact
 * is to a Local Identity, from which the Access Domain
 * is derived.
 *
 * The Access Key is the userid part of the Local Identity
 * if it is a user, or the +service if it is a service.
 * Aliases and signatures are not included in the Access Key,
 * because they are too variable for database lookups.
 *
 * Attributes (Rule Variables) are used to constrain and
 * override aliases or service arguments, and to impose
 * minimum requirements on the signatures flags.
 */


/** @brief Access Rights for Communication
 * 
 * The access rights for communication each specify a level
 * that the remote party should land it.  This may be any
 * combination of white, black and grey list and there is
 * even a definition that suggests landing the remote in a
 * honeypot, which might be useful in attack situations.
 *
 * Normally, a request would be interpreted as being on any
 * one of these levels, and indeed, the highest available
 * can be selected in these cases.
 *
 * @def ACCESS_COMM_WHITELIST
 * @brief Marks accepted communication
 *
 *      Just like
 *	the other communication rights, this is agnostic to
 *	the communication protocol being used, so whitelist
 *	entries for email and chat are all the same.  This
 *	is thought to make sense and minimise the burden of
 *	contact management on the communication ACL.
 *      (This maps to ACCESS_WRITE.)
 *
 * @def ACCESS_COMM_GRAYLIST
 * @brief An alias for ACCES_COMM_GREYLIST
 *
 * @def ACCESS_COMM_GREYLIST
 * @brief Marks communication that should be subjected to an extra check
 *
 *      Think of the
 *	grey-listing by temporarily refusing to accept email,
 *	in a manner that is protocol-specific and only as an
 *	suggested approach.  It is up to the actual protocol
 *	to decide how to handle gray-listing.
 *	(This maps to ACCESS_CREATE -- as it can create a new
 *	 access privilege.)
 *
 * @def ACCESS_COMM_BLACKLIST
 * @brief This marks refused communication
 *
 *      This tends
 *	to be the cautious default.  Users who welcome general
 *	communication may set the selector "@." to pass anyone,
 *	and still be able to add rejection for more specific
 *	askers.
 *	(This maps to ACCESS_VISITOR, a rather useless right.)
 *
 * @def ACCESS_COMM_HONEYPOT
 * @brief This marks communication for side-tracking to a honeypot
 *        or other active form of suppression or investigation.
 *
 *      This may be used for remotes who are known offensive and are
 *	ideally side-tracked into a trap, or a honeypot or other
 *	structure in support of observation.  This may not be
 *	possible everywhere, in which case it may be treated
 *	just like ACCESS_COMM_BLACKLIST.
 *	(This maps to ACCESS_KNOW -- the hosted identity would be
 *	 shown to the remote party.)
 */
#define ACCESS_COMM_WHITELIST  ACCESS_WRITE
#define ACCESS_COMM_GRAYLIST   ACCESS_CREATE
#define ACCESS_COMM_GREYLIST   ACCESS_CREATE
#define ACCESS_COMM_BLACKLIST  ACCESS_VISITOR
#define ACCESS_COMM_HONEYPOT   ACCESS_KNOW
//
#define ACCESS_COMM_WHITELIST_UP  ACCESS_WRITE_UP
#define ACCESS_COMM_GRAYLIST_UP   ACCESS_CREATE_UP
#define ACCESS_COMM_GREYLIST_UP   ACCESS_CREATE_UP
#define ACCESS_COMM_BLACKLIST_UP  ACCESS_VISITOR_UP
#define ACCESS_COMM_HONEYPOT_UP   ACCESS_KNOW_UP
//
#define ACCESS_COMM_WHITELIST_DOWN  ACCESS_WRITE_DOWN
#define ACCESS_COMM_GRAYLIST_DOWN   ACCESS_CREATE_DOWN
#define ACCESS_COMM_GREYLIST_DOWN   ACCESS_CREATE_DOWN
#define ACCESS_COMM_BLACKLIST_DOWN  ACCESS_VISITOR_DOWN
#define ACCESS_COMM_HONEYPOT_DOWN   ACCESS_KNOW_DOWN


/** @brief Attributes for Communication
 *
 * Communication does not only define rights, but keeps
 * variables as well, to provide alongside callbacks
 * sharing these rights with the application.  These
 * variables are declared as part of the ACL text, in
 * which they occur as single-letter assignments.
 *
 * ACCESS_COMM_ALIAS or ACCESS_COMM_ARGS can be used to
 *	match the existence and value of user alias
 *	and/or service arguments.  The value is matched
 *	for a given userid or service name, so which
 *	applies is known when this is declared.  The
 *	text does not start with a '+' but it may
 *	cover multiple levels by haing internal '+'
 *	separators.  The local identity that is being
 *	addressed may add more alias words after this
 *	one, by adding even more '+' separators in the
 *	aliases field.
 *
 * ACCESS_COMM_NEWALIAS or ACCESS_COMM_NEWARGS can be used
 *	to overwrite the alias and/or service args.
 *	This can be used to redirect traffic to a
 *	sub-user or sub-service that is better suited
 *	to the request.  The original aliases or args
 *	vanish.
 *
 * ACCESS_COMM_SIGFLAGS can be used to require minimal
 *	support of certain signature flags, as well as
 *	to demand that a signature is actually present
 *	in the addressed local identity.  The check is
 *	on the presence of flags alone; the signature
 *	validation should independently be made, using
 *	the routine that silently skips over missing
 *	signatures; the combination with this check is
 *	useful to nonetheless enforce a signature, and
 *	require it to be good (and even personalised).
 *	The format of this field is decimal, 0x-prefixed
 *	hexadecimal or 0-prefixed octal and its value
 *	is a bitwise-OR of required A2ID_SIGFLAG_ values.
 *
 */
#define ACCESS_COMM_ALIAS      RULES_VARINDEX ('a')
#define ACCESS_COMM_ARGS       RULES_VARINDEX ('a')
#define ACCESS_COMM_NEWALIAS   RULES_VARINDEX ('o')
#define ACCESS_COMM_NEWARGS    RULES_VARINDEX ('o')
#define ACCESS_COMM_SIGFLAGS   RULES_VARINDEX ('s')
#define ACCESS_COMM_NEWUSER    RULES_VARINDEX ('n')
#define ACCESS_COMM_NEWSERVICE RULES_VARINDEX ('n')
#define ACCESS_COMM_ACTOR      RULES_VARINDEX ('g')


/** @brief Access Types for Communication, as registered
 * on http://uuid.arpa2.org
 */
extern access_type access_type_comm;


/** @} */


/** @defgroup access_comm_h_ops Communication Access Operations
 * @{
 * Communication Access can define Rules in two ways, namely
 * as a Localised Ruleset in connection to a Resource or in an
 * LDAP accessRule, or as Ruleset driven from a database.  The
 * two functions to implement these functions are similar to
 * the generic rules for Rules Processing, but they are more
 * specific:
 *
 *  - The Access Type is fixated to the one for Communication
 *  - The Access Name is derived from the Local Identity
 *  - The Local Identity can be changed by the Rules Variables
 *  - The Access Rigths are shown as a level, not as flags
 */


/** @brief Communication Access Right is a level.
 */
typedef enum {
	access_comm_whitelist = 200,
	access_comm_greylist  = 300,
	access_comm_blacklist = 400,
	access_comm_honeypot  = 500,
	access_comm_undefined = 600,
} access_comm_level;


/** @brief Process Communication Access Rules
 *
 * @param[in] remote is the ARPA2 Identity for the remote
 * contact trying to initiate incoming communication.
 * This is the Identity over which Iteration is done
 * (in case of database lookups) or which is matched
 * against ~selector (in explicit Rules).
 *
 * @param[in,out] local is the ARPA2 Identity for the local
 * contact being approached.  The Access Domain is assumed
 * to match the domain of this Identity.  The function may
 * alter any part of this identity if it is needed to get
 * onto the white list.
 *
 * @param[in] opt_svckey may be NULL or otherwise provides
 * the Service Key.  NULL requests the default Service Key,
 * to be derived from the domain in \a local and without a
 * Database Secret.
 *
 * @param[in] svckeylen specifies the lenght of \a opt_svckey
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[in] opt_acl may be NULL to perform Iteration on
 * the \a remote to search the database for an ACL Ruleset
 * or, if this parameter is not NULL, it will be used instead.
 *
 * @param[in] acllen specifies the length of \a opt_acl
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[out] out_list indicates the list into which this
 * communication is to be sorted.  It is even safe to
 * interpret this output on failure.  The results may
 * be minimal in such cases.
 *
 * @param[out] optout_actor may be NULL to avoid Actors,
 * but will otherwise be filled with a Actor Identity
 * if a valid `=g<scene>+<actor>` attribute is related to
 * the white list entry.  The test `!a2act_isempty()` can be
 * used to test that an Actor was supplied.  This may for
 * example be a group member, or a local name to be used while
 * forwarding communication.  Generally, the sender address changes
 * from \a remote to \a optout_actor while relaying communication to
 * a next-stage delivery address that \a local expands to.
 *
 * @returns This function returns false with a com_err in
 * errno in case of technical problems.  If out_list should
 * be further processed, the function returns true.
 */
bool access_comm (const a2id_t *remote, a2id_t *local,
			const uint8_t *opt_svckey, unsigned svckeylen,
			const char *opt_acl, unsigned acllen,
			access_comm_level *out_level,
			a2act_t *optout_actor);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_ACCESS_COMM_H */

/** @} */
/** @} */
