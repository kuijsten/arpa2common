/* ARPA2 Block Cipher wrappers.
 *
 * Very simple wrappers that enable us to switch between backends.
 * Functions and symbols are prefixed with a2bc_ and A2BC_.
 *
 * The functions support authentication including additional data.
 * If this is desired, such data is supplied as non-NULL value in
 * the encryption or decryption calls.
 *
 * These wrappers are static inline functions.  This allows using a
 * different instantiation in different files, by defining a single
 * symbol A2BC_STYLE before including this file.  If it seems more
 * efficient to avoid repeated code, then it may actually be a good
 * idea to define functions that are meaningful to the _application_
 * and wrap those around these _technically_ useful inliners.
 *
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_CIPHER_H
#define ARPA2_CIPHER_H


#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

#include <arpa2/except.h>


#ifdef __cplusplus
extern "C" {
#endif


/* Block encryption operation, with authentication that may even
 * include additional data.  Provide output buffer NULL to sample
 * the size of the encrypted data.  The size provided is never
 * exceeded, but its value will always be replaced with the actual
 * data size, even if that is more than the buffer space.  This
 * provides support for sampling buffer size requirements.
 *
 * The key must be the same as used during a2bc_decrypt();
 * The in/inlen data is plaintext data to be encrypted.
 * The out/outlen data is ciphertext under the block cipher.
 * The auth/authlen is plaintext additional data added into
 *	the authentication of the block.  It should match the
 *	data that will be provided during a2bc_decrypt().
 *
 * The function returns true on success and false on failure.
 */
static inline bool a2bc_encrypt (
		const uint8_t *key /* A2BC_KEY_SIZE */,
		const uint8_t *in, int inlen,
		uint8_t *opt_out, int *outlen,
		const uint8_t *opt_auth, int authlen);


/* Block decryption operation, with authentication that may even
 * include additional data.  Provide output buffer NULL to sample
 * the size of the encrypted data.  The size provided is never
 * exceeded, but its value will always be replaced with the actual
 * data size, even if that is more than the buffer space.  This
 * provides support for sampling buffer size requirements.
 *
 * The key must be the same as used during a2bc_encrypt();
 * The in/inlen data is ciphertext under the block cipher.
 * The out/outlen data is plaintext data to was decrypted.
 * The auth/authlen is plaintext additional data added into
 *	the authentication of the block.  It should match the
 *	data that was provided during a2bc_encrypt().
 *
 * The function returns true on success and false on failure.
 */
static inline bool a2bc_decrypt (
		const uint8_t *key /* A2BC_KEY_SIZE */,
		const uint8_t *in, int inlen,
		uint8_t *opt_out, int *outlen,
		const uint8_t *opt_auth, int authlen);



#ifndef A2BC_STYLE_DEFAULT
//LATER// #define A2BC_STYLE_DEFAULT A2BC_STYLE_LIBSSL_AES128GCM
#warning "A2BC_STYLE_DEFAULT set to A2BC_STYLE_NULL which is dangerous"
#define A2BC_STYLE_DEFAULT A2BC_STYLE_NULL
#endif

#ifndef A2BC_STYLE
#define A2BC_STYLE A2BC_STYLE_DEFAULT
#endif





/***** NULL STYLE ENCRYPTION *****/



#if A2BC_STYLE == A2BC_STYLE_NULL


#warning "A2BC_STYLE_NULL selected: ___no___encryption___ or validation will be used"

#define A2BC_KEY_SIZE 1


static inline bool a2bc_encrypt (
				const uint8_t *key /* A2BC_KEY_SIZE */,
				const uint8_t *in, int inlen,
				uint8_t *opt_out, int *outlen,
				const uint8_t *opt_auth, int authlen) {
	//
	// Null encryption: Simply copy and/or propagate size
	bool ok = (inlen >= 0) && (inlen <= *outlen);
	if (ok && (opt_out != NULL)) {
		memcpy (opt_out, in, inlen);
	}
	*outlen = inlen;
	return ok;
}


static inline bool a2bc_decrypt (
				const uint8_t *key /* A2BC_KEY_SIZE */,
				const uint8_t *in, int inlen,
				uint8_t *opt_out, int *outlen,
				const uint8_t *opt_auth, int authlen) {
	//
	// Null decryption: Simply copy and/or propagate size
	bool ok = (inlen >= 0) && (inlen <= *outlen);
	if (ok && (opt_out != NULL)) {
		memcpy (opt_out, in, inlen);
	}
	*outlen = inlen;
	return ok;
}



/***** LIBSSL WITH AES-128 under GCM *****/


#elif A2BC_STYLE == A2BC_STYLE_LIBSSL_AES128GCM

/* Minimal include files for block cipher with OpenSSL's AES-128 under GCM */
#include <openssl/evp.h>


#error "TODO: A2BC_STYLE_LIBSSL_AES128GCM"




/***** NO MESSAGE DIGEST STYLE SELECTED *****/


#else /* A2BC_STYLE */


#error "Invalid value for A2BC_STYLE or A2BC_STYLE_DEFAULT"


#endif /* A2BC_STYLE */



/***** GENERIC DEFINITIONS *****/


#ifdef __cplusplus
}
#endif



#endif /* ARPA2_CIPHER_H */

