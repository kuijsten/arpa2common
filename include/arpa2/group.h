/** @defgroup group_h Group Rules for ARPA2 Identities
 * @{
 * Our blog introduces
 * [Groups as a form of Identity](http://internetwide.org/blog/2015/04/23/id-3-idforms.html)
 * and discusses backgrounds in the
 * [Identity series](http://internetwide.org/tag/identity.html).
 *
 * We define an efficient mechanism to iterate over
 * members, and alter addresses while traffic passes
 * through.  These addresses <group>+<member>@<domain>
 * can protect the privacy of group members while they
 * are communicating in the group.  The privacy does
 * not reach a level where administrators could not
 * trace abuse, however.
 *
 * This is a minimal abstraction over a framework for
 * Policy Rules that also serves Access Control.  It
 * uses the <domain> as Access Domain, has a registered
 * Access Type 5a1a2596-1763-36bf-a7b2-814ad98083ca and
 * sets the Access Name to <group>.  Triggers are used
 * to provide ^<member>@<address> where the <address>
 * is the delivery address for the member, as well as
 * the address they can use to contact the group.  The
 * Policy Rules also define flags with the properties
 * for the triggers that follow; members may not all
 * read all traffic and some may have special functions.
 */

/* SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef ARPA2_GROUP_H
#define ARPA2_GROUP_H


#include <stdbool.h>
#include <stdint.h>

#include "arpa2/identity.h"
#include "arpa2/rules.h"
#include "arpa2/access.h"


#ifdef __cplusplus
extern "C" {
#endif


/** @defgroup group_h_types Group Types
 * @{
 * Groups define a number of types.  Several of them
 * translate to Access Control or Rules abstractions.
 */


/** @brief The Rules Type for a group.
 *
 * This is the registered UUID value 5a1a2596-1763-36bf-a7b2-814ad98083ca
 * for use with groups.  See http://uuid.arpa2.org/ for details.
 */
extern rules_type rules_type_group;


/** @brief Marks of Group Member.
 */
typedef rules_flags group_marks;


/** Configuration Flags for Group Members.
 *
 * GROUP_RECV marks members who receive group data by default
 *
 * GROUP_SEND marks members who are prone to send to the group
 *
 * GROUP_ROBOT marks a command processor for the group; when it also
 *	defines GROUP_RECV then it may also index or archive content
 *
 * GROUP_MODERATOR marks a human moderator for the group
 *
 * GROUP_ADD marks those who may add new members
 *
 * GROUP_DEL marks those who may remove current members
 *
 * GROUP_OPERATOR marks the right to start or stop group service,
 *	without dropping its member list; this could be used to
 *	suspend and resume group service
 *
 * GROUP_KNOW marks members who allow a test for their existence
 *	as a group member, without concern about the delivery address
 *
 * GROUP_PROVE marks those who allow tests to see if a member name
 *	has a certain delivery address behind it
 *
 * GROUP_VISITOR marks members who will receive traffic from non-members
 */
#define GROUP_RECV       ACCESS_READ
#define GROUP_SEND       ACCESS_WRITE
#define GROUP_ROBOT      ACCESS_SERVICE
#define GROUP_MODERATOR  ACCESS_ADMIN
#define GROUP_ADDMEMBER  ACCESS_CREATE
#define GROUP_DELMEMBER  ACCESS_DELETE
#define GROUP_OPERATE    ACCESS_OPERATE
#define GROUP_KNOW       ACCESS_KNOW
#define GROUP_PROVE      ACCESS_PROVE
#define GROUP_VISITOR    ACCESS_VISITOR


/** @} */


/** @brief Callback Function for Group Processing.
 *
 * This callback function prototype is used in group_iterate()
 * and will be called for all group members that look like they
 * should receive delivery of the intended communication.  It is
 * up to the callback implementation to decide if a fork of the
 * communication is actually created for the recipient.
 *
 * The sender is described with its Group Identity as supplied
 * in the original call.  The recipient is described by their
 * group member name (the alias part after '+') and its coupled
 * delivery address is also given.
 *
 * @param [in,out] updata is a data structure into which state
 * can accumulate between callbacks.
 *
 * @param [in] marks holds the GROUP_xxx marks for the current
 *	\a recipient and \a delivery addresses.
 *
 * @param [in] sender is the sender address.  This is a group member
 *	address, which usually differs from the Remote Identity
 *	that was used in the Communication Access lookup.
 *
 * @param [in] recipient is the recipient address.  This is a
 *	group member address, which usually differs from the
 *	delivery address.
 *	
 * @param [in] delivery is the delivery address for the
 *	\a recipient.  It cannot be assumed to be terminated
 *	with a NUL character.
 *
 * @param [in] deliverylen is the length of the \a delivery
 *	string.
 *
 * @returns For future compatibility, this function must always
 *	return true.
 */
typedef bool group_iterate_upcall (
		void *updata,
		group_marks marks,
		const a2act_t *sender,
		const a2act_t *recipient,
		const char *delivery,
		unsigned deliverylen);


/** @} */



/** @defgroup group_h_functions Group Functions
 * @{
 * Groups defines functions to help working with them.
 */


/** @brief Initialise the ARPA2 Group system.
 *
 * This has a mirror image for cleaup in group_fini().
 */
void group_init (void);


/** @brief Finalise the ARPA2 Group system.
 *
 * This may do cleanup operations.
 */
void group_fini (void);


/** @brief Process the Ruleset defined for a Group.
 *
 * Iterate over a group and attempt delivery to each
 * member's delivery address, including the sender's.
 *
 * @param [in] Group Member Identity for the sender, as
 * retrieved from access_comm().  The userid part is used
 * as the group name and the single +alias is the member
 * name for the sender on the list.
 *
 * @param [in] recipients member addresses, consisting
 * of the group name, zero or more members and possibly
 * +-+ to switch to suppression of recipients.  This is
 * an array, to allow one iteration to match multiple
 * recipients member addresses.  When multiple match,
 * there will still be only one delivery suggested.
 *
 * @param [in] filters points to a NULL-terminated array
 * of strings that each represent an alternative filter
 * that might pass members during iteration.  The filter
 * strings each contain a sequence "+mid" strings, with
 * "+-+" to toggle from accepting to rejection (or back)
 * within a filter string.  The filter string ends at a
 * NUL or "@" character.  When no "+mid" occurs in a
 * filter string it will pass all members marked with
 * GROUP_RECV.  Though it is possible to insert a group
 * name without "+" before the first "+mid", this may
 * end up as a bug when group names contain a "+" as is
 * formally permitted -- so don't do that.
 *
 * @param [in] required_marks are marks that cause a
 * member to be skipped when they miss any of these marks.
 *
 * @param [in] forbidden_marks are marks that cause a
 * member to be skipped when marked with either of these.
 *
 * @param [in] upcall is the callback function that is
 * invoked for each member, presenting the member alias
 * and its delivery address, along with the markings
 * for the entry.
 *
 * @param [in] updata is the data supplied as accumulator
 * to the \a upcall callback function.
 *
 * @param[in] opt_svckey may be NULL or otherwise provides
 * the Service Key.  NULL requests the default Service Key,
 * to be derived from the domain in \a local and without a
 * Database Secret.
 *
 * @param[in] svckeylen specifies the lenght of \a opt_svckey
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[in] opt_rules may be NULL to perform Iteration on
 * the \a remote to search the database for an ACL Ruleset
 * or, if this parameter is not NULL, it will be used instead.
 *
 * @param[in] ruleslen specifies the length of \a opt_rules
 * but is only meaningful if that parameter is not NULL.
 *
 * @returns This function returns true on success, which
 * implies that some ruleset was found, even when the marks
 * or other selective criteria did not cause a callback.
 * Failures are of a technical nature, and return false
 * while setting errno to a com_err code.  This includes
 * having found no group members, in which case errno is
 * set to A2XS_ERR_MISSING_RULESET.
 */
bool group_iterate (const a2act_t *sender, const char **filters,
		group_marks required_marks, group_marks forbidden_marks,
		const uint8_t *opt_svckey, unsigned svckeylen,
		const char *opt_rules, unsigned ruleslen,
		group_iterate_upcall upcall, void *updata);


/** @brief Determine the rights of a Group Member.
 *
 * This call can be made to determine if an Actor Identity matches the
 * ruleset for a group, and has a rule that defines its identity.  For
 * that specific identity, the marks are harvested and returned.  When
 * no marks are found, the GROUP_VISITOR marking is returned.  When no
 * ruleset is found, meaning that the group does not exist, the same
 * marks are set but an error is returned with the Common Error code
 * A2XS_ERR_MISSING_RULESET.
 *
 * @param[in] potential_member the Actor Identity, parsed with
 * one alias level, that should be tested to be a Group Member.
 *
 * @param[in] opt_svckey may be NULL or otherwise provides
 * the Service Key.  NULL requests the default Service Key,
 * to be derived from the domain in \a local and without a
 * Database Secret.
 *
 * @param[in] svckeylen specifies the lenght of \a opt_svckey
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[in] opt_rules may be NULL to perform Iteration on
 * the \a remote to search the database for an ACL Ruleset
 * or, if this parameter is not NULL, it will be used instead.
 *
 * @param[in] ruleslen specifies the length of \a opt_rules
 * but is only meaningful if that parameter is not NULL.
 *
 * @param[out] out_marks returns the marks for the potential_member.
 * This is set to GROUP_VISITOR when the member was not found or
 * when any error occurs, including not having found the group.
 *
 * @returns true on success, or false with a Common Error code
 * in case of problems.  Problems include the non-existence of
 * the group, which is signaled as A2XS_ERR_MISSING_RULESET,
 * but not the non-existence of the member.
 */
bool group_hasmember (const a2act_t *potential_member,
		const uint8_t *opt_svckey, unsigned svckeylen,
		const char *opt_rules, unsigned ruleslen,
		group_marks *out_marks);


/** @} */

#ifdef __cplusplus
}
#endif

#endif /* ARPA2_GROUP_H */


/** @} */
