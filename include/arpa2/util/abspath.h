/** @defgroup Absolute paths in File Systems
 * @{
 * This is a portable definition for absolute path testing.
 */

/*
 * SPDX-FileCopyrightText: Copyright 2021 Rick van Rein <rick@openfortress.nl>
 * SPDX-License-Identifier: BSD-2-Clause
 */


#ifndef _ARPA2_UTIL_ABSPATH
#define _ARPA2_UTIL_ABSPATH


#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @def is_abspath(path)
 *
 * @brief Test whether this is an absolute file system path.
 *
 * This macro has versions for POSIX and Windows, and adapts to
 * the context in which it compiles.
 *
 * For Windows, drive letters are assumed to be ASCII letters,
 * so isalpha() is avoided because that depends on locale.
 *
 * @param path The string to be tested.  If it is NUL-terminated
 * you can rely on it to not look beyond the end of the string.
 * (You may also opt for supplying enough characters; POSIX only
 * requires 1 character, Windows required at least 3 characters).
 *
 * @return true for absolute paths, false otherwise
 */

#ifndef _WIN32

static inline bool is_abspath (const char *path) {
	return (path [0] == '/');
}

#else /* _WIN32 */

static inline bool is_abspath (const char *path) {
	if ((*path < 'A') || (*path > 'z')) {
		return false;
	}
	if ((*path > 'Z') && (*path < 'a')) {
		return false;
	}
	if (path [1] != ':') {
		return false;
	}
	if ((path [2] != '\\') && (path [2] != '/')) {
		return false;
	}
	return true;
}

#endif /* _WIN32 */


#ifdef __cplusplus
}
#endif


#endif /* _ARPA2_UTILS_ABSPATH */

/** @} */
