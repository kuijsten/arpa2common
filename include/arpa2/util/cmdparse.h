/** @defgroup arpa2cmdparse ARPA2 Command Parsing
 * @{
 * Parse commandlines of <keyword> <value> pairs, possibly
 * preceded by <class> <action> selectors.
 *
 * In addition, support a format with a <keyword> <value> pair
 * per line, followed by an empty line.  This format can be used
 * to save/load or to send/recv configuration data.
 *
 * Applications call cmdparse_xxx functions to perform work, and
 * reference data structures that hold all the required information.
 */

/* SPDX-License-Identifier: BSD-2-Clause
 * SPDX-FileCopyrightText: Copyright 2020 Rick van Rein <rick@openfortress.nl>
 */


#ifndef ARPA2_UTIL_CMDPARSE
#define ARPA2_UTIL_CMDPARSE


#include <stdint.h>
#include <stdbool.h>


#ifdef __cplusplus
extern "C" {
#endif


/** @brief Grammar definition for <keyword> <value> pairs.
 *
 * Parse up to 32 different keywords.  The words are in the position
 * in the output array.  Non-parsed entries are set to NULL.
 * The FLAG_kwd bits in listwords mark keywords that welcome values
 * with spaces to separate list items, usable with parse_word().
 *
 * Recognise combinations until their list ends with 0.
 * See parse_combinations() for details.
 */
struct cmdparse_grammar {
	const char *keywords [32];
	const uint32_t listwords;
	const uint32_t *combinations;
};


/** @brief Parsed value structure from <keyword> <value> pairs.
 *
 * Parse and bind up to 32 different values, at indexes marked by
 * keywords.  The structure must be initialised with zeroes and then
 * the grammar pointer should be set.  At that point, the parser can
 * be used to pile up keyword/value pairs.  When done, a test for
 * the occurring combinations completes parsing.  This can be done
 * in one stroke with the NEW_CMDPARSER(_grammar) macro below.
 */
struct cmdparser {
	const struct cmdparse_grammar *grammar;
	char *values [32];
	char *backstore;
	uint32_t combination;
	bool error;
};


/** @brief Action structure for <class> <action> [<keyword> <value>]...
 *
 * Reference cmdparse_grammar for the trailing part, and assume
 * being referenced from a cmdparse_class.
 *
 * The @a actionfun is only called when parsing succeeded; it should
 * return true on success and false on failure.
 *
 * An array of these structures usually defines available <action>
 * strings, and is ended with a zeroed structure.
 *
 * The usage string is intended as support during syntax errors
 * beyond this <action>.
 */
struct cmdparse_action {
	const char *actionstr;
	short opcode;
	const struct cmdparse_grammar *grammar;
	const char *usage;
	bool (*actionfun) (short opcode, struct cmdparser *prs, const char *usage, int argc, char *argv []);
};


/** @brief Class structure for <class> <action> [<keyword> <value>]...
 *
 * Reference cmdparse_action for the trailing part, and assume
 * being the top-level reference for parsing a commandline.
 *
 * An array of these structures usually defines available <class>
 * strings, and is ended with a zeroed structure.
 *
 * The usage string is intended as support during syntax errors
 * beyond this <class>.
 */
struct cmdparse_class {
	const char *classstr;
	const char *usage;
	const struct cmdparse_action *actions;
};


/** @brief Initialise a new command parser.
 *
 * Given a grammar, produce the fields that initialise a fresh
 * cmdparser instance structure.  This is a good start for the
 * actual parsing to take place.
 */
#define NEW_CMDPARSER(_grammar) { .grammar = (_grammar), .values = { NULL }, .combination = 0, .error = false, .backstore = NULL }



/** @brief Add a keyword/value pair to a parser structure.
 *
 * Return true on success or false on failure for this isolated step.
 * Parsing may continue; failures accumulate in the parser structure.
 * Or you may use:  ok = ok && cmdparse_keyword_value (...);
 */
bool cmdparse_keyword_value (struct cmdparser *prs, char *keyword, char *value);


/** @brief Check the combinations as a sequence of mask/value pairs ended by 0.
 *
 * Run this after parsing keyword arguments into the parser structure.
 *
 * Whether a masked value _i matches the actual combination of parsed
 * bits depends on whether prs->combination & mask_i == value_i.
 * When it matches, the mask_i value is ORed into an overall score.
 *
 * The goal for correct syntax is to reach an overall score that matches
 * the ORed value of all mask_i in the combinations.  Note that overal
 * between mask_i and mask_j values can be used to enumerate alternatives.
 *
 * A few usage hints:
 *  - allow everything with just a single 0 (so no mask_i and value_i pair)
 *  - make one mask_i == value_i with all required keyword argument flags
 *  - do not mention flags for completely optional choices
 *  - only mix flags to express non-orthogonal combinations
 *  - for non-orthogonal combinations, enumerate their values
 *  - to fixate dependent argument options, include the dependent flags
 *
 * This function returns true when the combinations agree.
 */
bool cmdparse_combinations (struct cmdparser *prs);


/** @brief Parse a list of <keyword> <value> pairs.
 *
 * Parse <keyword> <value> pairs from an argv[] array with argc entries,
 * starting at argv [0].  Require an even number in argc.
 * The tests of cmdparse_combinations() are included.
 * Return true on success.
 */
bool cmdparse_kwargs (struct cmdparser *prs, int argc, char **argv);


/** Recognise a word within a larger text.
 *
 *  Move a string pointer to the next word, and return its length:
 *  - Add the previous length (initially this should be 0)
 *  - Add the number of spaces and tabs
 *  - At the end of line, return 0
 *  - Otherwise, return the length until space, tab or newline
 *
 * Note that an empty line returns length 0, as does the string end.
 *
 * Typical use in iteration:
 *
 *      unsigned wlen = 0;
 *      char *words = "... ... ...";
 *      while (wlen = cmdparse_word (&words, wlen), wlen > 0) {
 *              printf ("Word: \"%.*s\"\n", wlen, words);
 *      }
 */
unsigned cmdparse_word (const char **str, unsigned prevlen);


/** @brief Parse the presented text string.
 *
 * This must be formatted as produced by cmdparse_unparse(),
 * with pretty column formatting.
 *
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool cmdparse_string (struct cmdparser *prs, char *text);


/** @brief Parse the textual backstore content.
 *
 * The backstore content must be formatted as produced by
 * cmdparse_unparse(), with pretty column formatting.
 *
 * The return value is an individual success, and information is
 * logged and accumulated in the parser object.  Note that the
 * parsed format ends in an empty line to mark completeness.
 *
 * This function updates the backstore with NUL characters to
 * separate keywords and values.  Parsing again would fail.
 */
bool cmdparse_backstore (struct cmdparser *prs);


/** @brief Fill a string buffer with <keyword> <value> lines.
 *
 * The buffer ends with a NUL character.
 *
 * The return value is like for snprintf(), and indicates the number of
 * characters excluding the NUL terminator.  Also like snprintf(), writes
 * never run outside the buffer space.  You can safely supply a length 0
 * and/or a buffer NULL to only determine the number of characters needed:
 *
 *	struct cmdparser *prs = ...fill.data...;
 *	unsigned txtsz = cmdparse_unparse (prs, NULL, 0);
 *	unsigned bufsz = txtsz + 1;
 *      char buf [bufsz];
 *	cmdparse_unparse (prs, buf, bufsz);
 *	fputs (bufsz, stdout);
 *
 * The produced string ends in an empty line, which can be used to check
 * that a request arrived completely, rather than having been cut short
 * in transmission.
 */
unsigned cmdparse_unparse (const struct cmdparser *prs, char *opt_buf, unsigned buflen);


/** @brief Print usage constraints.
 *
 * Present a context-specific usage string, the last parsed argv[] index
 * (1 for class, 2 for class actions) and the argv array.
 */
void cmdparse_usage (const char *usage, int argi, char *argv []);


/** @brief Parse command arguments as <class> <action> [<keyword> <value>]...
 *
 * This uses a 2-level structure of cmdparse_class referencing
 * cmdparse_action and finally a grammar for <keyword> <value>
 * pairs, along with a processing function to invoke on success.
 *
 * The first level is an array pointed at by clsarray, and continues
 * until a zeroed entry.  The second level is referenced from the
 * first level structure, and indicates another array with a zeroed
 * terminator.  Each level presents a usage string to apply when it
 * parses successfully.  Only when nothing parses, the provided usage
 * string will be used.
 *
 * In case of syntax errors, a usage message is displayed (the initial
 * value should be provided).  Otherwise, the processing function
 * is called.  If either fails, false is returned; if both succeed,
 * true is returned.
 */
bool cmdparse_class_action (int argc, char *argv [],
			const char *usage, const struct cmdparse_class *clsarray);


#ifdef __cplusplus
}
#endif

#endif /* ARPA2_UTIL_CMDPARSE */

/** @} */

