.TH A2RULE 8 "June 2021" "ARPA2.net" "System Management Commands"
.SH NAME
a2rule \- Manage the ARPA2 Policy Rule database
.SH SYNOPSIS
.PP
.B a2rule
group add|set [ \fIdbsetup\fR ]
.RS
member \fIa2group\fR+\fImember\fR@\fIa2domain\fR
.br
identity \fIremote@domain\fR [marks \fImarks\fR]
.RE
.PP
.B a2rule
group del|get [ \fIdbsetup\fR ]
.RS
member \fIa2group\fR+\fImember\fR@\fIa2domain\fR
.RE
.PP
.B a2rule
comm add|set [ \fIdbsetup\fR ]
.RS
local \fIa2user\fR@\fIa2domain\fR remote \fIremote@domain\fR
.br
( list \fIlist\fR | rights \fIrights\fR )
.br
[alias|endalias \fIalias\fR] [signflags \fInumber\fR]
.br
[newuserid \fInewuserid\fR] [newalias \fInewalias\fR] [actorid \fIactorid\fR]
.RE
.PP
.B a2rule
comm del|get [ \fIdbsetup\fR ]
.RS
local \fIa2user\fR@\fIa2domain\fR remote \fIremote@domain\fR
.br
( list \fIlist\fR | rights \fIrights\fR )
.br
[alias|endalias \fIalias\fR] [signflags \fInumber\fR]
.RE
.PP
.B a2rule
docu add|set [ \fIdbsetup\fR ]
.RS
domain \fIa2domain\fR ( collection \fIcolluuid\fR
.br
                | volume \fIvolume\fR [path \fIpath\fR] )
.br
remote \fIremote@domain\fR rights \fIrights\fR [actorid \fIactorid\fR]
.RE
.PP
.B a2rule
docu del|get [ \fIdbsetup\fR ]
.RS
domain \fIa2domain\fR ( collection \fIcolluuid\fR
.br
                | volume \fIvolume\fR [path \fIpath\fR] )
.br
remote \fIremote@domain\fR
.RE
.PP
.B a2rule
rules add|set [ \fIdbsetup\fR ] dbsvckey \fIhexkey\fR
.RS
[type \fIrules_type\fR] name \fIrules_name\fR
.br
[selector \fIa2selector\fR] rule \fIpolicy_rule\fR
.RE
.PP
.B a2rule
rules get|del [ \fIdbsetup\fR ] dbsvckey \fIhexkey\fR
.RS
[type \fIrules_type\fR] name \fIrules_name\fR
.br
[selector \fIa2selector\fR] [rule \fIpolicy_rule\fR]
.RE
.SH DESCRIPTION
The command
.BR a2rule
is used to make changes in the ARPA2 Policy Rule database.
Such a central database helps to coordinate changes to
Access Control or Group Management, and to automate the
updates to such settings.  Administrators can use
.B a2rule
for manual changes to the central Policy Rule database.
.PP
A customary assumption for Access Control logic is that any
identities passed into it are authenticated.  Applications
that forego such precaution may be lied to so they select
permissive Access Rules; furthermore, there is a
serious risk that such applications become an oracle to
be probed for Access Rights.
.PP
The Policy Rule database stores things that may be done
by certain remotes, during particular usage patterns.
For Access Control, it concentrates on who may do
what to which resource.  Resources may be such elements
as users to connect to (Communication Access) or
documents to see or edit (Document Access).
There are also Policy Rules for Groups, allowing to
setup and iterate their members and the mappings from
each to their delivery address.
.PP
Policy Rules are designed for fast automated processing,
and though they follow a somewhat readable ASCII format
it would be error-prone to write it, especially because
each use of rules adds its own quirks.  The purpose of
.BR a2rule
is to write well-formed rules into the database, and
avoid the anxiety of having to debug this format.
.SH "DATABASE SETUP"
.PP
The \fIdbsetup\fR general parameters allow changes to the
database access from the default.  They are listed below.
Normally, the database is found in a set location and
used without protective secret.
.TP
.BI "dbtrunk " num
provides the trunk number, which identifies the origin
of a key-value mapping.  Applications ignore trunks and
simply take the first that comes along, but for bulk
management it can be used to indicate an origin of the
database rules, and that may be handy when the need
arises to erase or restart the database input from a
certain source without hampering any others.
The default trunk number 0 is advised for manual
operations.
.TP
.BI "dbprefix #" word
provides a non-standard prefix word to \fBdel\fR and
\fBget\fR operations.  The standard prefix for \fBa2rule\fR
is \fB#a2xs\fR.  Such prefix word are of no meaning during
rule processing, but they usually help to select among tools
from which rules originate.  Rules inserted by other tools
than \fBa2rule\fR can be seen and even cleaned up, but
they should not be (over)written, because that might damage
assumptions made in these other tools.  Use those other
tools to produce fresh rule values if so desired, under the
consistency observations of those tools.
.TP
.BI "dbdomkey " hexkey
provides the Domain Key from which domain admins may access
settings for their domain without seeing those in others.
This is a lesser grant than the \fBdbsecret\fR provides.
(You cannot provide a Domain Key as well as a Service Key.)
.PP
There are a fer environment variable to control the
database setup:
.TP
\fBARPA2_RULES_DIR\fR
sets the directory that serves as the environment for the
ARPA2 Policy Rules database.  When not set, the tool uses a
built-in default like
.IR /var/lib/arpa2/rules .
This variable is not a property of
.B a2rule
but of the ARPA2 Common library, so it works consistently
across applications using the library.  In other words,
the setting of this environment variable can switch all
uses of ARPA2 Policy Rules to another database.
.TP
\fBARPA2_DOMAINKEY_\fIdomain\fR
holds the Domain Key in hexadecimal form.  This may be setup
in the environment for a domain administrator.  The inclusion
of the \fIdomain\fR helps to support multiple domains in a
single administrator account.  An example variable would be
\fBARPA2_DOMAINKEY_example_org\fR for domain \fBexample.org\fR.
.br
The domain name is mapped to make it suitable for an
environment variable:  Letters are mapped to lowercase,
letters and digits pass, dots turn to underscores and
dashes turn to \fBX\fR characters.  Anything else is
ignored, including UTF-8 specialties.  This is not
expected to be problematic in practice.
.br
The administrator account should be protected from dumping
those environment variables; online services should not set
them and at most configure a Service Key.
.TP
\fBARPA2_SERVICEKEY\fR
holds the Service Key in hexadecimal form.  This may be setup
in an environment that runs a particular service, be it a
large daemon or a partial program on its behalf.  It is used
in the \fBruleset\fR subcommand.
.br
This variable may change or disappear in future versions,
because its value varies with Access Domain and Access Type,
but neither have to be provided and therefore a single
variable name is used.  This reduces the practical use of
this variable.
.PP
While running
.BR a2rule ,
the key derivation process is shown, which should help to
give an idea what the parameters do at the various levels.
The levels of interest are:
.TP
.B Domain Key
hashed from the \fIdbsecret\fR and the domain name.
This can be used by domain admins, while the general
\fIdbsecret\fR remains the property of their provider.
.TP
.B Service Key
hashed from the Domain Key and the Access Type, which
separates access types \fBcomm\fR, \fBdocu\fR and
\fBgroup\fR with completely different keys.  This can be
used by service providers, who install it in a (virtual)
service for a given domain.
.TP
.B Selector Key
hashed from the Service Key, Access Name and Remote
Selector, this is used to look for a rule set in the
database.  When the Remote Selector is subjected to
iteration as described below, a few Selector Keys will
be derived and looked up.  The fact that
.B a2rule
shows precisely one Service Key is because it works on
individual rule sets, without iteration.  Services would
normally iterate to find the most concrete rule set that
is defined.
.PP
Future extensions with encrypted database values may
protect Rule content, which may be useful when
data records are sent from an Identity Provider to a
Service Provider, where the latter gets to data only on
a need-to-know basis, so only for an actual request by
a user.  For now, the database scheme assumes that the
data does not travel between data processing industries.
.SH "ARPA2 GROUPS"
Groups are a domain facility that collects internal and
external identities; identities are usually
.I remote@domain
addresses that may be used in a variety of protocols;
ARPA2 Groups are not specific to any one protocol, but
can be mapped to various existing practices, including
email lists, chat rooms and conference calls.  It is up
to applications to connect groups to their idea of
collective use.
.PP
Every identity involved in a group is assigned an
ARPA2 Actor Identity that looks like
.IB a2group + member @ domain
where the part
.IB a2group @ domain
is the ARPA2 Actor Identity for the entire group and every
.I member
alias references one group member.  The underlying
identity of the member does not need to be shown to
address some or all members, and when communicating
with a group the sender therefore does not need to
surrender his address.  Members only need to know
one another by their ARPA2 Actor Identity annex
ARPA2 Group Member Identity; but it is
eventually an implementer's choice and perhaps also
dependent on protocols inhowfar this privacy is shielded.
There is support for non-member access to ARPA2 Groups,
by mapping them to a dynamic member address.
.PP
Groups consist of a rule set, with a key that is influenced
by the \fIa2domain\fR name, the Access Type for ARPA2 Groups and
the \fIa2group\fR serving as the Access Name.  You can create
a separate rule for each ARPA2 Group Member with
.B a2rule
operations
.BR add ,
.BR del ,
.BR set
and
.BR get ,
which end up being set updating operations.  The value in the
set looks like
.PP
.BI "#a2xs %" marks " ^" member "@" remote@domain
.PP
where \fB#a2xs\fR marks this tool and \fIremote\fR@\fIdomain\fR
marks the (generally considered remote) identity.  To access
group facilities, users authenticate under the latter form, but
implementations of ARPA2 Groups should aim to store and log
mostly the
.IR a2group + member @ domain
form, so the Actor Identity called ARPA2 Group Member Identity
in the context of ARPA2 Groups.  This provides privacy but,
as the administrator sees fit, the rules can be queried to
retrieve an abuser's remote identity.
.PP
The programming interface to ARPA2 Groups is mostly through
the
.BR group_iterate ()
operation, which delivers callbacks for any member that matches
selection criteria.  The criteria may be part of recipient
addresses, but there may also be applications that want to
hard-wire them.  It is generally assumed that the sender or
requesting ARPA2 Identity is already an member identity, which
is usually achieved through Access Control, as described below.
.SH "ARPA2 COMMUNICATION ACCESS"
The purpose of ARPA2 Communication Access is to grant recipients
maximum control over who may communicate with them, regardless
of the protocol being used.  This means that same access control
applies to email, chat and telephony.
.PP
The remote is assumed to provide an address, which is considered
the remote address and be of a form
.IR remote @ domain ,
in addition to the destined address, which will be parsed as
.IR a2userid [+ aliases ][+ signature +]@ a2domain .
The parts \fI+aliases\fR and \fI+signature+\fR are optional,
as indicated by rectangular brackets.  Alternatively, when
the destination address starts with a plus it is considered
a service under ARPA2 Identity mangement, and parsed as
.RI + a2service [+ svcargs ][+ signature +]@ a2domain .
Normally, the +\fIa2service\fR is processed like
\fIa2userid\fR and \fIsvcargs\fR like \fIaliases\fR.
.PP
Communication Access iterates over the remote address from
concrete to abstract.  It first removes the user identity,
and then removes one label from the domain name at a time,
for instance:
.PP
.B john+cook@example.com
.br
.B @example.com
.br
.B @.com
.br
.B @.
.PP
The last form is the most abstract, and it matches every
remote address.  The strings produced thusly are known as
ARPA2 Selectors, and form a pattern to match ARPA2 Identities.
One might think of an ARPA2 Selector as the set of all
ARPA2 Identities that find it during iteration towards the
general form.  The ARPA2 Selector \fB@.\fR is always last,
so its set would hold all Remote Identities.
.PP
Access Rules are stored in the database under a key that is
determined by the Access Type for Communication Access, the
\fIa2domain\fR name, possibly a database key and the
selector (which is iteratively generalised as indicated above).
The Access Name is the \fIa2userid\fR or the \fB+\fIa2service\fR.
Note that the \aliases\fR, \fIsvcargs\fR and \fIsignature\fR
parts are not part of the lookup key.  The result is a key
that is higly specific, yet only calls for a few key lookups
in the LMDB database to decide on Communication Access.
This schedule scales well to large operations, because the
key lookup in a database is a very efficient operation.
The section
.I Data Structures and Constants for Rules and RuleDB
in the documentation (derived from
.BR "<arpa2/rules_db.h>" )
gives the calculations made to optimise the design choices
for large-scale operations without interfering with modest
situations.
.PP
Communication Access stores a rule set under the keys, but
empty sets do not exist.  This means that a key/value lookup
only succeeeds when a non-empty ruleset is found, which is
then processed without regard for any more abstract keys.
This means that the most concrete form is leading.  Since
this is a rule set, it is possible to modify the rules
stored there.  This can be done with the
.B a2rule
operations
.BR add ,
.BR del
and
.BR get ,
which end up being set management operations.  A rule
for Communication Access can be as simple as
.PP
.BI "#a2xs %" rights
.br
or as feature-packed as
.br
.BI "#a2xs =a" aliases " =s" sigflags " =n" newuserid " =o" ovraliases " =g" actor " %" rights
.PP
The values for \fIrights\fR overlap with \fIlist\fR, namely
\fB%W\fR for \fBwhite\fR listing,
\fB%C\fR for \fBgrey\fR listing,
\fB%V\fR for \fBblack\fR listing and
\fB%K\fR for \fBhoneypot\fR condemnation.
The rule format does not represent the list form, so it is encoded as
\fB%\fIrights\fR instead.
.PP
The following keyword-value pairs locate an Access Rule in the database:
.TP
.BI "alias " alias
adds \fB=a\fIalias\fR to the rule to require that these
match the beginning of the \fIaliases\fR or \fIsvcargs\fR
in the local ARPA2 Identity.  This is used to configure
separate Communication Access rules for aliases.  Longer
\fIaliases\fR than the matched \fIalias\fR are permitted,
as long as they start with \fB+\fR.
.TP
.BI "endalias " alias
adds \fB=a\fIalias\fB@\fR to the rule.  This variation
does not match longer \fIaliases\fR aliases in the local
ARPA2 Identity than this option's \fIalias\fR.
The specal form \fB=a@\fR indicates that the local ARPA2
Identity must not hold any aliases, which is requested
with an \fBendalias ""\fR option.
.TP
.BI "signflags " number
adds \fB=s\fInumber\fR to the rule to require the
\fIsignature\fR to be present in the local ARPA2 Identity
and have no zero bits overlapping one bits in the mask
given as a \fInumber\fR in this option.  Signature flags
exist to demand such things as an exiration day, the
remote domain and the remote user.  The values of the bits
are derived by the
.BR a2id_parse ()
operation on the local destination address, and turned into
a numeric value.  This value is setup as \fIsigflags\fR here,
in decimal form.  Note the difference in the format from
the destination address, which uses base32 notation.
The signature itself is not validated, as that is normally
done by the application through a call to
.BR a2id_verify (),
but the question whether a signature is present and what its
constraints on the context of use are is decided with this
attribute.
.PP
The following keyword-value pairs serve to modify the lookup
in case of white listing:
.TP
.BI "newuserid " newuserid
adds \fB=n\fInewuserid\fR to the rule to indicate that the
local ARPA2 Identity will be changed from the original
\fIa2userid\fR to this \fInewuserid\fR.
Any aliases in the original address are removed, but any
signature is kept.
.TP
.BI "newalias " overalias
adds \fB=o\fIoveralias\fR to the rule to indicate that the
local ARPA2 Identity will be set to these aliases.  Any
aliases in the original address are removed, but any
signature is kept and this option does not modify the
\fIa2userid\fR or \fB+\fIservice\fR either.
.TP
.BI "actorid " \fIactorid\fR
adds \fB=g\fIactorid\fR to the rule, to be extended with
\fB@\fIa2domain\fR to form an Actor Identity that is
returned to the access-requesting application.  Unlike the
.BR =n and =o
attributes, this one does not alter the local ARPA2 Identity
but it provides an alter ego that can be used alongside it,
or possibly instead of it, as the application sees fit.
Actor Identities are prominently used for Group Members,
which is why the attribute name
.B =g
stuck.
.PP
To process Communication Access in applications, they call
.BR access_comm ().
This takes a remote sender and a local destination address.
The local address may be altered by the call.  In addition, there is an
output parameter for an Actor Identity that overtakes from
the remote sender for local classification purposes.  Since
this is the result of Communcation Access, it is possible
to have a different Actor Identity for different access
patterns; notably, when addressing an ARPA2 Group there is
an option of assigning (remote) members with their group
member Actor Identity.
.SH "ARPA2 DOCUMENT ACCESS"
An important purpose of Access Control is to stop users from
accessing documents or resources that they must not access.
This is the general idea behind Document Access.  Local users
are treated as if they are remote, with facilities to setup
rules for the local \fIa2domain\fR than are more permissive
than for other domains.
.PP
The things that may be accessed are named by a path.  These
paths can point to directories or files on an operator-defined
volume.  Volumes might have names like "home" or perhaps even
"john@home".
.PP
Aside from those operator-defined volumes, there is an
implicit document naming structure called Reservoir.  This is
a two-level structure local to a domain, with a directory
represented as a UUID to serve as a Resource Collection or
directory, with a second UUID to zoom in on a specific
Resource or file.  The metadata for Reservoir is stored in
LDAP to make it easily searchable with descriptive attributes.
.PP
A third concept in Reservoir is a Resource Index, which is
a mapping from names to Resource Collections, much like a
symbolic link in a file sysem.  Indexes usually have a
default Collection.  Every domain can have an Index and every
user under a domain can have an Index; these constructs
define a default Collection to domains and their users, plus
a mapping from names to alternate Collections.  Names are a
human thing; for Reservoir they always map to a single
Collection UUID.  Nesting is just as meaningless to the
computer, because all Collection UUIDs sit side by side.
Paths of name lookups in Indexes may look like they traverse
to certain depths, but they translate to side-way jumping
through the single level of Collection UUIDs.
.PP
Policy Rules for Document Access are incredibly simple.
The basic form is
.BI "#a2xs %" rights
.br
and the only feature currently defined is
.br
.BI "#a2xs =g" actor " %" rights
.PP
The following keyword-value pairs are used to locate a
Rule in the database:
.TP
.BI "domain " a2domain
specifies that \fIa2domain\fR serves is the Access Domain.
It is the scope under which both Reservoir and the \fBvolume\fR
form reside.
.TP
.BI "collection " colluuid
represents a collection in the domain's Reservoir in the
reproducible form of a UUID in lowercase text form.  Any
paths, domains or users into the Reservoir are transformed
to their \fIcolluuid\fR value.  Document Access in Reservoir
is always and only at the Resource Collection level, and
normally applied only to the last element of a traversed path,
so the form is always \fB/\fIcolluuid\fB/\fR with no extra
charactes added.
.TP
.BI "volume " volume
is an operator-selected name for a \fIvolume\fR.  There
cannot be \fB/\fR characters in a volume name, but the
\fB@\fR character can be safely used.  It has no formal
meaning but may be a pleasant form to represent user-specific
(parts of) volumes.
.TP
.BI "path " path
is a path on the volume.  Any leading \fB/\fR characters
are removed.  An empty path adds an empty string after
the \fB//\fIvolume\fB/\fR or \fB/\fIcolluuid\fB/\fR strings.
Non-empty paths ending in \fB/\fR should be used to indicate
a directory while any other ending character should indicate
a file.
.TP
.BI "remote " \fIremote\fB@\fIdomain\fR
specifies the Remote Selector that should match with the
(local or) remote user who wants to gain access to the
described resource or document.
.PP
The following options are used to influence the output from
Document Access:
.TP
.BI "actorid " actorid
adds \fB=g\fIactorid\fB@\fIa2domain\fR to the rule to indicate
that access should be granted under that Actor Identity.  This
may be used to indicate a document owner by an ARPA2 Group,
which is accessible to Group Members who use their member alias
under the \fIa2domain\fR.  This would influence visibility to
others, including such things as document audit logs, and thus
aid the privacy of the Group Members.
.TP
.BI "rights " rights
adds \fB%\fIrights\fR to the rule to indicate the rights for
(local or) remote users matchings this Policy Rule.
.PP
.SH "ARPA2 POLICY RULES"
The foregoing sections detailed applications of the general
framework for ARPA2 Policy Rules.  It is possible to perform
each of the foregoing operations at an abstract level by
interacting with the ruleset directly.  This means that the
specifics of the application logic are no longer concealed,
but there are also more possibilities, albeit constrained to
a level where a Service Key must be presented.
.PP
The following arguments can be provided on the commandline
for interaction with Policy Rules:
.TP
.BI "dbsvckey " hexkey
provides the Service Key available to the service for which
the request is being made.  When not present, it will be
loaded from an environment variable \fBARPA2_SERVICEKEY\fR.
.TP
.BI "name " rules_name
provides the Rules Name, which is an UTF-8 string with a
grammar specific to the Rules Type.  This grammar is only
checked when the \fBtype\fR argument is present.
.TP
.BI "rule " policy_rule
provides a specific Policy Rule, used in set operations
\fBadd\fR, \fBdel\fR, \fBset\fR or \fBget\fR.  When not
provided in \fBdel\fR or \fBget\fR, the operation will
be applied to all rules in the ruleset.
.TP
.BI "type " rules_type
may optionally be provided to improve the interaction.  It
can setup a grammar to check the \fBname\fR, thereby
avoiding notational variations that could make services
in different implementations incompatible.

.SH AUTHOR
.PP
Written by Rick van Rein of OpenFortress.nl, for the ARPA2.net project.
.SH "REPORTING BUGS"
For any discussion, including about bugs, please use the
.IR https://gitlab.com/arpa2/arpa2common/
project page for ARPA2 Common.  We use its issue tracker
to track bugs and feature requests.
.SH COPYRIGHT
.PP
Copyright \(co 2021 Rick van Rein, ARPA2.net.
.PP
ARPA2 is funded from InternetWide.org, which in turn receives
funding from various sources.  Part of this work was funded
through a grant of SIDNfonds, a public interest fund geared
towards a better Internet for all, and paid by administration
fees for .nl domain names.  Other parts were funded by
NLnet, a public interest fund geared towwards open source
excellence, and by the NGI Pointer fund of the European
Union.
.SH "SEE ALSO"
ARPA2 Common is documented on
.IR http://common.arpa2.net/
and its code is on
.IR https://gitlab.com/arpa2/arpa2common/
